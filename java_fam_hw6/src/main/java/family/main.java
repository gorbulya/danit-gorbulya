package family;

public class main {

    public static void main(String[] args) {

        Family family = new Family(new Human("Artem", "Ivanov", 35 ), new Human("Galina", "Ivanova", 36, 81));

//       for (int i = 0; i < 50000; i++) {
//           Human human1 = new Human("Artem", "Ivanov", 35 );
//       }


            Human child = new Human("Maxim", "Ivanov", 16);
        Human child2 = new Human();
        Human child3 = new Human("Petya", "Ivanov", 5, 30);
        Human child4 = new Human("MAria", "Ivanova", 5, 80, new String[][]{{DayOfWeek.MONDAY.name(),"пора в школу"}, {DayOfWeek.THURSDAY.name(),"пора на кружок"}});


        Pet dog = new Dog( "Rex", 9, 30, new String[] {"есть", "спать", "играть"});

        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        family.setPet(dog);

        System.out.println(family);
        System.out.print("Количество человек в семье: ");
        System.out.println(family.countFamily());

        family.deleteChild(child2);

        System.out.println(family);
        System.out.print("Количество человек в семье: ");
        System.out.println(family.countFamily());

        child2.feedPet(true, dog);
        child2.greetPet(dog);
        child2.describePet(dog);

        dog.eat();
        dog.respond();
        dog.foul();

        family.getMother().feedPet(true, dog);
        family.getMother().feedPet(false, dog);
        family.getMother().feedPet(false, dog);
        family.getMother().feedPet(false, dog);


        Man man = new Man("Stas", "Danilov", 16);
        Woman woman = new Woman("Nata", "Danilova", 18);
        man.repairCar();
        man.greetPet(dog);
        woman.makeup();
        woman.greetPet(dog);
    }

}
