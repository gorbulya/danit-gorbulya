package family;

public class RoboCat extends Pet {

    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("I am robodog!!");
    }
}
