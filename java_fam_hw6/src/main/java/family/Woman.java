package family;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, String[][] schedule) {
        super(name, surname, year, iq, schedule);
    }

    void makeup() {
        System.out.println("Make up");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("HI, " + pet.getNickname());
    }
}
