package family;

import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.Arrays;

public class FamilyTest {

    Family family = new Family(new Human("Artem", "Ivanov", 35 ), new Human("Galina", "Ivanova", 36, 81));

    Human child1 = new Human("Maxim", "Ivanov", 16);
    Human child2 = new Human("Petya", "Ivanov", 5, 30);
    Human child3 = new Human("MAria", "Ivanova", 5, 80, new String[][]{{DayOfWeek.MONDAY.name(),"пора в школу"}, {DayOfWeek.THURSDAY.name(),"пора на кружок"}});
    Pet dog = new Dog( "Rex", 9, 30, new String[] {"есть", "спать", "играть"});

    @Before
    public void setAll() {
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.setPet(dog);
    }

    public String getExpectedToString(Family family) {
        return "Family{" + "\n" +
                "mother=" + family.getMother() + "\n" +
                ", father=" + family.getFather() + "\n" +
                ", children=" + Arrays.toString(family.getChildren()) + "\n" +
                ", pet=" + family.getPet() + "\n" + "}";
    }

    @Test
    public void testToString(){
        assertEquals(family.toString(), getExpectedToString(family));
    }

    @Test
    public void testCountFamily() {
        int resultCount = family.countFamily();
        family.addChild(new Human());
        assertEquals(resultCount+1, family.countFamily());
        family.addChild(new Human());
        assertEquals(++resultCount+1, family.countFamily());
    }

    @Test
    public void testAddChildPositive() {
        int resultBefore = family.getChildren().length;
        Human newChild = new Human("Nazar", "Ivanov", 34);
        family.addChild(newChild);

        assertEquals(family.getChildren().length, resultBefore+1);
        assertEquals(family.getChildren()[family.getChildren().length-1], newChild);
    }

    @Test
    public void testDeleteChildPositive() {
        int resultBefore = family.getChildren().length;
        family.deleteChild(child2);
        assertEquals(family.getChildren().length, resultBefore-1);

        for (Human child : family.getChildren()) {
            assertNotSame(child, child2);
        }
    }

    @Test
    public void testDeleteChildNegative() {
        String resultBefore = Arrays.toString(family.getChildren());
        family.deleteChild(new Human("Injdsjjsd", "asdasdlasd", 56));
        assertEquals(resultBefore, Arrays.toString(family.getChildren()));
    }

    @Test
    public void testDeleteChildIndexPositive() {
        int resultBefore = family.getChildren().length;
        boolean b = family.deleteChild(1);
        assertEquals(family.getChildren().length, resultBefore-1);
        assertEquals(b,true);

        for (Human child : family.getChildren()) {
            assertNotSame(child, child2);
        }
    }

    @Test
    public void testDeleteChildIndexNegative() {
        int resultBefore = family.getChildren().length;
        String resultBeforeStr = Arrays.toString(family.getChildren());
        boolean b = family.deleteChild(-1);
        assertEquals(family.getChildren().length, resultBefore);
        assertEquals(b,false);
        assertEquals(resultBeforeStr, Arrays.toString(family.getChildren()));
    }








}
