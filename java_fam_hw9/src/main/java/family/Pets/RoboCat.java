package family.Pets;

import family.Pets.Pet;

import java.util.Set;

public class RoboCat extends Pet {

    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("I am robodog!!");
    }
}
