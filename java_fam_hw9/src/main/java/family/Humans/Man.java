package family.Humans;

import family.Enum.DayOfWeek;
import family.Pets.Pet;

import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname) {
        super(name, surname);
    }

    public Man(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void repairCar() {
        System.out.println("Repair CAr");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hello, " + pet.getSpecies());
    }


}
