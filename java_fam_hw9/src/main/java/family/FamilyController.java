package family;

import family.Humans.Family;
import family.Humans.Human;
import family.Pets.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int number) {
        familyService.getFamiliesBiggerThan(number);
    }

    public void getFamiliesLessThan(int number) {
        familyService.getFamiliesLessThan(number);
    }

    public void countFamiliesWithMemberNumber(int number) {
        familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human human1, Human human2) {
        familyService.createNewFamily(human1, human2);
    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String nameMan, String nameWoman) {
        return familyService.bornChild(family, nameMan, nameWoman);
    }

    public Family adoptChild(Family family, Human human) {
        return familyService.adoptChild(family, human);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int indexFamily) {
        return familyService.getFamilyById(indexFamily);
    }

    public Set<Pet> getPets(int indexFamily) {
        return familyService.getPets(indexFamily);
    }

    public void addPet(int indexFamily, Pet pet) {
        familyService.addPet(indexFamily, pet);
    }

}
