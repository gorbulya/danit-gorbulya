package family;


import family.Humans.Family;
import family.Humans.Human;
import family.Humans.Man;
import family.Humans.Woman;
import family.Pets.Dog;
import family.Pets.Pet;
import org.junit.Assert;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.*;

public class FamilyServiceTest {

    List<Family> families = new ArrayList<>();
    Human child1 = new Man("Vlad", "Ivanov", "01/03/2016");
    Human child2 = new Man("Nick", "Petrov", "01/03/2011");
    Human child22 = new Man("Igor", "Petrov", "01/03/2006");
    Human child3 = new Man("Denis", "Sidorov", "01/03/2001");
    Human child33 = new Man("Roma", "Sidorov", "01/03/1996");

    Human woman = new Woman("Elena", "Ivanova", "01/03/1981");
    Human man = new Man("Dima", "Ivanov", "01/03/1976");
    Family family = new Family(woman, man);

    Human woman1 = new Woman("Kate", "Petrova", "01/03/1976");
    Human man1 = new Man("Bob", "Petrov", "01/03/1971");
    Family family1 = new Family(woman1, man1);

    Human woman2 = new Woman("Anne", "Sidorova", "01/03/1971");
    Human man2 = new Man("Oleg", "Sidorov", "01/03/1966");
    Family family2 = new Family(woman2, man2);

    Set<Pet> pets = new HashSet<>();
    Pet dog = new Dog("Tuzik", 10, 10, null);
    Pet dog1 = new Dog("Bobik", 10, 10, null);

    @Before
    public void setUp() {
        family.addChild(child1);
        family1.addChild(child2);
        family1.addChild(child22);

        families.add(family);
        families.add(family1);

        pets.add(dog);
    }

    public List<Family> getExpectedToString(Family family, Family family1) {
        List<Family> families = new ArrayList<>();
        families.add(family);
        families.add(family1);
        return families;
    }

    public String getExpectedToStringList(Family family, Family family1) {
        return "List of all families: \n 1. " + family + "\n" + "2. " + family1;
    }

    @Test
    public void getAllFamiliesPositive() {
        Assert.assertEquals(getExpectedToString(family, family1).toString(), "[" + family + ", " + family1 + "]");
    }

    @Test
    public void getAllFamiliesNegative() {
        Assert.assertNotEquals(getExpectedToString(family, family1).toString(), "[" + family1 + ", " + family1 + "]");
    }

    @Test
    public void getFamiliesBiggerThanPositive() {
        int countFamilyBiggerThan = 3;
        int result = countFamilyBiggerThan + 1;
        Assert.assertEquals(result, family1.countFamily());
    }

    @Test
    public void getFamiliesBiggerThanNegative() {
        int countFamilyBiggerThan = 3;
        int result = countFamilyBiggerThan + 1;
        Assert.assertNotEquals(result, family.countFamily());
    }

    @Test
    public void getFamiliesLessThanPositive() {
        int countFamilyLessThan = 4;
        Assert.assertEquals(countFamilyLessThan - 1 , family.countFamily());
    }

    @Test
    public void getFamiliesLessThanNegative() {
        int countFamilyLessThan = 4;
        Assert.assertNotEquals(countFamilyLessThan - 1 , family1.countFamily());
    }

    @Test
    public void countFamiliesWithMemberNumberPositive() {
        int countPeople = 3;
        Assert.assertEquals(countPeople, family.countFamily());
    }

    @Test
    public void countFamiliesWithMemberNumberNegative() {
        int countPeople = 3;
        Assert.assertNotEquals(countPeople, family1.countFamily());
    }

    @Test
    public void createNewFamilyPositive() {
        Assert.assertEquals(getExpectedToString(family, family2).toString(),
                "[" + family + ", " + new Family(new Woman("Anne", "Sidorova", "01/03/1971"), new Man("Oleg", "Sidorov" ,"01/03/1966")).toString() + "]");
    }

    @Test
    public void createNewFamilyNegative() {
        Assert.assertNotEquals(getExpectedToString(family, family1).toString(),
                "[" + family + ", " + new Family(new Woman("Anne", "Sidorova", "01/03/1971"), new Man("Oleg", "Sidorov", "01/03/1966")).toString() + "]");
    }

    @Test
    public void deleteFamilyPositive() {
        List result = getExpectedToString(family, family1);
        result.remove(family1);
        Assert.assertEquals("[" +family.toString() + "]",  result.toString());
    }

    @Test
    public void deleteFamilyNegative() {
        List result = getExpectedToString(family, family1);
        result.remove(family);
        Assert.assertNotEquals("[" +family.toString() + "]",  result.toString());
    }

    @Test
    public void bornChildPositive() {
        int countPeople = family.countFamily();
        Human human = new Man("Artur", "Lietun","01/03/2016");
        family.addChild(human);
        Assert.assertEquals(countPeople + 1, family.countFamily());
    }

    @Test
    public void bornChildNegative() {
        int countPeople = family.countFamily();
        Human human = new Man("Artur", "Lietun", "01/03/2016");
        family1.addChild(human);
        Assert.assertNotEquals(countPeople + 1, family.countFamily());
    }

    @Test
    public void adoptChildPositive() {
        int countPeople = family.countFamily();
        Human human = new Man("Artur", "Lietun", "01/03/2016");
        family.addChild(human);
        Assert.assertEquals(countPeople + 1, family.countFamily());
    }

    @Test
    public void adoptChildNegative() {
        int countPeople = family.countFamily();
        Human human = new Man("Artur", "Lietun", "01/03/2016");
        family1.addChild(human);
        Assert.assertNotEquals(countPeople + 1, family.countFamily());
    }

    @Test
    public void deleteAllChildrenOlderThenPositive() {
        family.addChild(child2);
        List<Family> resultBefore = getExpectedToString(family, family1);
        family.getChildren().remove(child2);
        List<Family> resultAfter = getExpectedToString(family, family1);
        Assert.assertEquals(resultBefore, resultAfter);
    }

    @Test
    public void countPositive() {
        Assert.assertEquals(2, families.size());
    }

    @Test
    public void countNegative() {
        Assert.assertNotEquals(3, families.size());
    }

    @Test
    public void getFamilyByIdPositive() {
        Assert.assertEquals(family, families.get(0));
    }

    @Test
    public void getFamilyByIdNegative() {
        Assert.assertNotEquals(family, families.get(1));
    }

    @Test
    public void getPetsPositive() {
        family.setPet(pets);
        String expectedResult = "[" + dog.toString() + "]";
        Assert.assertEquals(expectedResult, family.getPet().toString());
    }

    @Test
    public void getPetsNegative() {
        pets.add(dog1);
        family.setPet(pets);
        String expectedResult = "[" + dog.toString() + "]";
        Assert.assertNotEquals(expectedResult, family.getPet().toString());
    }


    @Test
    public void addPetPositive() {
        family.setPet(pets);
        String expectedResult = family.getPet().toString();
        expectedResult = "[" + dog1 + ", "+ expectedResult.substring(1);
        pets.add(dog1);
        Assert.assertEquals(expectedResult, family.getPet().toString());
    }

    @Test
    public void addPetNegative() {
        family.setPet(pets);
        String expectedResult = family.getPet().toString();
        expectedResult = "[" + dog + ", "+ expectedResult.substring(1);
        pets.add(dog1);
        Assert.assertNotEquals(expectedResult, family.getPet().toString());
    }










}
