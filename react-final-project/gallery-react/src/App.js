import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css"

function App() {

  const images = [
    {
      original: 'https://picsum.photos/id/1027/1000/',
      thumbnail: 'https://picsum.photos/id/1027/250/',
    },
    {
      original: 'https://picsum.photos/id/338/1000/',
      thumbnail: 'https://picsum.photos/id/338/250/',
    },
    {
      original: 'https://picsum.photos/id/342/1000/',
      thumbnail: 'https://picsum.photos/id/342/250/',
    },
    {
      original: 'https://picsum.photos/id/349/1000/',
      thumbnail: 'https://picsum.photos/id/349/250/',
    },
    {
      original: 'https://picsum.photos/id/447/1000/',
      thumbnail: 'https://picsum.photos/id/447/250/',
    },
  ];

  return (
    <div className="App">
        <ImageGallery items={images} 
                      showNav={true}
                      showThumbnails={true}
                      thumbnailPosition={"bottom"}
                      showFullscreenButton={false}
                      showPlayButton={false}
                      showBullets={true}
                      showIndex={true}
                      />
    </div>
  );
}

export default App;
