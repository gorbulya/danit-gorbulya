//версия с code

document.addEventListener("DOMContentLoaded", ready);

function ready() {

    let buttonCollection = document.getElementsByClassName("btn");
    buttonCollection = Array.prototype.slice.call(buttonCollection);

    window.addEventListener("keyup", (event) => {

        let keyPressButton = event.key;

        buttonCollection.forEach((elem) => {

            if (keyPressButton.toLowerCase() ===  elem.innerHTML.toLowerCase()){
                elem.classList.add("blue");
            } else {
                elem.classList.remove("blue");
            }
        })
    })
}