// Дан массив books.
//     const books = [
//     {
//         author: "Скотт Бэккер",
//         name: "Тьма, что приходит прежде",
//         price: 70
//     },
//     {
//         author: "Скотт Бэккер",
//         name: "Воин-пророк",
//     },
//     {
//         name: "Тысячекратная мысль",
//         price: 70
//     },
//     {
//         author: "Скотт Бэккер",
//         name: "Нечестивый Консульт",
//         price: 70
//     },
//     {
//         author: "Дарья Донцова",
//         name: "Детектив на диете",
//         price: 40
//     },
//     {
//         author: "Дарья Донцова",
//         name: "Дед Снегур и Морозочка",
//     }
// ];

// Выведите этот массив в виде списка (тег ul).
// На странице должен находится div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в обьекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветится ошибка с указанием - какого свойства нету в обьекте.
//     Тот обьект, что некорректен по условиям предыдущего пункта - не должен появиться на странице.

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


function arrayToList(arr) {
    let mapArray = `<ul> ${arr.map( function(num){
        
            try {
                if (!num.author)  {
                    throw new Error("В обьекте нет поля Автор");
                }
                if (!num.name) {
                    throw new Error("В обьекте нет поля Имя");
                }
                if (!num.price) {
                    throw new Error("В обьекте нет поля Цена");
                }
                
                return (`<li> Author: ${num.author} : Name: ${num.name} : Price: ${num.price} </li>`);
            }
            catch (e) {
                console.log( e.name, e.message);
            }
            
    }
        )} </ul>`;

        return mapArray;
}

function addToHtml (arr) {
    document.getElementById('root').innerHTML = arrayToList(arr).split(',').join('');
}

addToHtml(books);