// Задание
// Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
// Считать с помощью модального окна браузера число, которое введет пользователь (n).
// С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
// Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).

let fibbonachi0 = prompt("Введите натуриальное F0 число фиббоначи");

while ( fibbonachi0 === "" || fibbonachi0 === null || !Number.isInteger(+fibbonachi0) || fibbonachi0 === " " ) {
    fibbonachi0 = prompt("Это не натуральное число, Введите натуриальное F0 число фиббоначи");
}

let fibbonachi1 = prompt("Введите натуриальное F1 число фиббоначи");

while ( fibbonachi1 === "" || fibbonachi1 === null || !Number.isInteger(+fibbonachi1) || fibbonachi1 === " " ) {
    fibbonachi1 = prompt("Это не натуральное число, Введите натуриальное F1 число фиббоначи");
}

let fibbonachiN = prompt("Введите натуриальное N последовательности фиббоначи");

while ( fibbonachiN === "" || fibbonachiN === null || !Number.isInteger(+fibbonachiN) || fibbonachiN === " " ) {
    fibbonachiN = prompt("Это не натуральное число, Введите натуриальное F1 число фиббоначи");
}

fibbonachi0 = +fibbonachi0;
fibbonachi1 = +fibbonachi1;
fibbonachiN = +fibbonachiN;

// function fibbonachi(f0,f1,n) {
//     let a = f0;
//     let b = f1;
//     let c;
//     if (n>0) {
//         for (let i = 3; i <= n; i++) {
//             c = a + b;
//             a = b;
//             b = c;
//         }
//     return b;
//     } else if (n<0) {
//         for (let i = -1; i >= n; i--) {
//             c = b - a;
//             b = a;
//             a = c;
//
//         }
//      return c;
//     } else {
//         return a;
//     }
// }
//  сказали сделать рекурсией  :)

function fibbonachi(f0,f1,n){
    if ( n === 0 ) {
        return f0;
    } else if ( n === 1) {
        return f1;
    } else if ( n>1 ) {
        return fibbonachi(f0,f1,n-2)+fibbonachi(f0,f1,n-1)
    } else if ( n<0 ) {
        return fibbonachi(f0,f1,n+2)-fibbonachi(f0,f1,n+1)
    }
}


console.log(fibbonachi(fibbonachi0,fibbonachi1,fibbonachiN));