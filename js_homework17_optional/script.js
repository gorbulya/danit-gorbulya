// Задание
// Создать объект студент "студент" и проанализировать его табель. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// Создать пустой объект student, с полями name и last name.
// Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
// В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
// Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент переведен на следующий курс.
// Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.

const student = {};

let name = prompt("Введите ваше имя", "Иван");

while ( name === "" || name === null){
    name = prompt("Это не имя - Введите ваше имя");
}

student.name = name;

let lastName = prompt("Ведите вашу фамилию", "Иванов");

student.table = {};

while ( lastName === "" || lastName === null){
    lastName = prompt("Это не фамилия - введите вашу фамилию");
}

student.lastName = lastName;

while (true) {
    let subject = prompt("Введите название предмета");
    if (subject === null ) {
        alert("Fihish input");
        break;
    }
    student.table[subject] = prompt("Введите оценку предмета (1-12)");

}

let flag=1;
for (let key in student.table){
        if (student.table[key] < 4){
            flag=0;
            break;
        }
    }
if (flag === 1){
    alert("Нет оценок ниже 4 - Студент переведен на следующий курс");
}

let count = 0;
let summ = 0;
for (let key in student.table){
    summ += +student.table[key];
    count++;
}

let average=summ/count;

console.log(average);

if (average > 7) {
    alert("Средний балл " + average + " Студенту назначена стипендия");
} else {
    alert("Средний балл " + average + " Студенту не назначена стипендия");
}

console.log(student);