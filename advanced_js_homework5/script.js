// Написать программу "Я тебя по айпи вычислю" с использованием конструкции async/ await

// Технические требования:

// Создать простую HTML страницу с кнопкой Вычислить по IP.
// По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
// Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
// Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.

const _URL_IP = 'https://api.ipify.org/?format=json';
const _URL_INFO = 'http://ip-api.com/json/';

let button = document.getElementById('container__button');
let textBox = document.getElementsByClassName('container_text')[0];

button.addEventListener("click", getDataByIp);


async function getDataByIp() {

    try {
        const responseIp = await fetch(`${_URL_IP}`);
        const dataIP = await responseIp.json();
        const ip = dataIP.ip;
        const responseGeo = await fetch(`${_URL_INFO}${ip}?fields=continent,country,regionName,city,district`);
        const dataGeo = await responseGeo.json();
        let adress = `Континент: ${dataGeo.continent} / Страна: ${dataGeo.country} / Регион: ${dataGeo.regionName} / Город: ${dataGeo.city} / Район: ${dataGeo.district}`;
        textBox.innerHTML = adress;
    } catch (e) {
        console.error(e);
    }

}