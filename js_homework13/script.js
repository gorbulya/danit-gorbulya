
document.addEventListener("DOMContentLoaded", ready);

function ready() {

        let color = localStorage.getItem("currentTheme");
        console.log(color);
        let button = document.getElementsByClassName("button4")[0];
        let temp = document.getElementById("css-style");

        if (!color || color === "light") {
            switchToTheme("light");
        } else if (!color || color === "dark") {
            switchToTheme("dark");
        }

        button.addEventListener("click", ()=>{

            let color = localStorage.getItem("currentTheme");

            if (color === "light") {
                switchToTheme("dark");
            } else if (color === "dark") {
                switchToTheme("light");
            }


        });

    function switchToTheme(theme) {
        switch (theme) {
            case "light": {
                temp.setAttribute("href", "style/style.css");
                localStorage.setItem("currentTheme", "light");
                break;
            }
            case "dark": {
                temp.setAttribute("href", "style/style2.css");
                localStorage.setItem("currentTheme", "dark");
                break;
            }
        }
    }






}