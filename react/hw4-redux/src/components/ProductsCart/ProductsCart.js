import React from 'react';
import PropTypes from 'prop-types';
import Product from '../Product/Product';


const ProductsCart = (props) => {

	const { prods } = props;

	const countCart = prods.filter(item => item.isOnCart === true).length;
	const prod = prods.filter(item => item.isOnCart === true)
					  .map(item => (<Product key={item.id} {...item} />));


	if (countCart === 0) {
		return (
			<>
				<div className="products-wrapper">
					<div className="no-items">
						<div className="no-items__wrapper">
							Нет товаров для отображения
               				 </div>
					</div>
				</div>
			</>
		)
	} else {
		return (
			<>
				<div className="products-wrapper">
					{prod}
				</div>
			</>
		)
	}
}

ProductsCart.propTypes = {
	products: PropTypes.array,
}

ProductsCart.defaultProps = {
	products: [],
}

export default ProductsCart;