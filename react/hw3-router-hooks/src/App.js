import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Header from '../src/components/Header/Header';
import AppRoutes from '../src/routes/AppRoutes';
import { withRouter } from 'react-router-dom';



import './App.scss';

const App = (props) => {

  const [products, setProducts] = useState([]);

  useEffect(() => {

    if (localStorage.getItem('products') === null) {

      axios("/products.json")
        .then(response => setProducts(response.data));

    } else {

      setProducts(JSON.parse(localStorage.getItem('products')));

    }

  }, []);

  useEffect(() => {

    const prods = JSON.stringify(products);
    localStorage.setItem('products', prods);


  }, [products]);

  const toggleToFavourite = (id) => {

    setProducts(products.map(item => (id === item.id ? { ...item, isFavourite: !item.isFavourite } : item)));

  }

  const toggleToCart = (id) => {

    setProducts(products.map(item => (id === item.id ? { ...item, isOnCart: !item.isOnCart } : item)));

  }

  const clearLocalStorage = () => {
    localStorage.removeItem('products');
    axios("/products.json")
      .then(response => setProducts(response.data));

    props.history.push('/');
  }

  return (
    <>
      <Header clearLocalStorage={clearLocalStorage} products={products} />
      <AppRoutes products={products} toggleToFavourite={toggleToFavourite} toggleToCart={toggleToCart} />
    </>
  );

}

App.propTypes = {
  products: PropTypes.array,
}

App.defaultProps = {
  products: [],
}

export default withRouter(App);
