import React from 'react'
import {Route, Switch } from 'react-router-dom';
import ProductsWrapper from '../components/ProductsWrapper/ProductsWrapper';
import ProductsCart from '../components/ProductsCart/ProductsCart';
import ProductsFavorites from '../components/ProductsFavorites/ProductsFavorites';
import Page404 from '../components/Page404/Page404';

const AppRoutes = (props)=> {
      
    const {products, toggleToFavourite, toggleToCart} = props;

    return (
      <Switch>
        <Route  exact path='/' render={() => <ProductsWrapper products={products} toggleToFavourite={toggleToFavourite} toggleToCart={toggleToCart}/>} />
        <Route  exact path='/favorites' render={() => <ProductsFavorites products={products} toggleToFavourite={toggleToFavourite} toggleToCart={toggleToCart}/>}/>
        <Route  exact path='/cart' render={() => <ProductsCart products={products} toggleToFavourite={toggleToFavourite} toggleToCart={toggleToCart}/>}  />
        <Route path='*' component={Page404} />
      </Switch>
    )
}

export default AppRoutes;