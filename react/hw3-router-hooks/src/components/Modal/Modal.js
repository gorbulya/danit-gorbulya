import React from 'react';
import Button from '../Button/Button';

const Modal = (props) => {

    const { header, text, actions, closeButton, closeFunction } = props;

    return (
        <div className="wrapper-modal" onClick={closeFunction}>
            <div className="wrapper-modal__window">
                <div className="wrapper-modal__header">
                    <span>{header}</span>
                    {closeButton && <Button closeIcon={true} onClick={closeFunction} />}
                </div>
                <div className="wrapper-modal__content">
                    {text}
                </div>
                <div className="wrapper-modal__button">
                    {actions}
                </div>
            </div>
        </div>
    )
}

export default Modal;