import React from 'react';
import PropTypes from 'prop-types';
import Product from '../Product/Product';


const ProductsCart = (props) => {

	const { products, toggleToFavourite, toggleToCart } = props;

	const countFav = products.filter(item => item.isFavourite === true).length;

	const prod = products.filter(item => item.isFavourite === true).map(item => (<Product toggleToFavourite={toggleToFavourite} toggleToCart={toggleToCart} key={item.id} {...item} />));

	if (countFav === 0) {
		return (
			<>
				<div className="products-wrapper">
					<div className="no-items">
						<div className="no-items__wrapper">
							Нет товаров для отображения
               				 </div>
					</div>
				</div>
			</>
		)
	} else {
		return (
			<>
				<div className="products-wrapper">
					{prod}
				</div>
			</>
		)
	}

}

ProductsCart.propTypes = {
	products: PropTypes.array,
}

ProductsCart.defaultProps = {
	products: [],
}

export default ProductsCart;