import React, {PureComponent} from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';

import './App.scss';
import './components/Button/Button';
import './components/Modal/Modal';

class App extends PureComponent {

  state = {
		modal: null,
	}

  render (){

    return (
      <>
      <div className="content-wrapper">
        <Button backgroundColor="#006400" className="btn-open-modal" text="Open first modal" onClick={() => this.showModalFirst()}/>
        <Button backgroundColor="#B22222" className="btn-open-modal" text="Open second modal" onClick={() => this.showModalSecond()}/>
      </div>
      {this.state.modal}
      </>
    );

  }

  showModalFirst = () => {
		this.setState({
					modal: <Modal
              header="Do you want to delete this file?"
              text="Once you delete this file, it won’t be possible to undo this action. 
              Are you sure you want to delete it?"
              actions={[
                <Button key="1" backgroundColor="#B3382C" className="btn-modal" text="Ok" onClick={this.closeModal}/>,
                <Button key="2" backgroundColor="#B3382C" className="btn-modal" text="Cancel" onClick={this.closeModal}/>
              ]}
              closeButton={true}
              closeFunction= {this.closeModal}
              
          />
		});
  }
  
  showModalSecond = () => {
		this.setState({
					modal: <Modal
              header="I can not do it"
              text="I can’t do this it’s never possible"
              actions={[
                <Button key="1" backgroundColor="#B3382C" className="btn-modal" text="Cancel" onClick={this.closeModal}/>
              ]}
              closeButton={false} 
              closeFunction= {this.closeModal}
              
          />
		});
  }
  
  closeModal = (event) => {
		if (event.target === event.currentTarget) {
			this.setState({
				modal: null,
			})
		}
	}
  
}

export default App;
