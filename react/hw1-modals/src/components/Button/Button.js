import React, { PureComponent } from 'react'

class Button extends PureComponent {
	render() {
        
        const {text, className, backgroundColor, onClick, closeIcon} = this.props;

        if (closeIcon) {
            return (<div className="close-icon" onClick={onClick}>&#10006;</div>)
        }
		
		return (
			<button className={className} style={{backgroundColor: backgroundColor}} onClick={onClick} >{text}</button>
		)
	}
}

export default Button;