import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Product from '../Product/Product';

class ProductsWrapper extends PureComponent {

    render() {
        
		const {products, toggleToFavourite ,toggleToCart} = this.props;

        const prod = products.map(item => (<Product toggleToFavourite={toggleToFavourite} toggleToCart={toggleToCart} key={item.id} {...item} />));

		return (
			<div className="products-wrapper">
                {prod}
            </div>
		)
	}
}

ProductsWrapper.propTypes = {
	products: PropTypes.array,
}

ProductsWrapper.defaultProps = {
	products: [],
}

export default ProductsWrapper;