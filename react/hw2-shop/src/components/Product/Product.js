import React, { PureComponent } from 'react';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';

import '../Button/Button';
import '../Modal/Modal';



class Product extends PureComponent {

  state = {
    modal: null
  }

  render() {

    const { id, color, title, price, image, isOnCart, isFavourite, toggleToFavourite} = this.props;

    return (
      <>
        <div className="product">
          <div className="product__photo">
            <img src={`/products/${image}`} alt="" />
            <i className={`product__favorite fas fa-star ${isFavourite ? "color--active" : ""}`} onClick={() => toggleToFavourite(id)}></i>
          </div >
          <div className="product__title">
            {title}
          </div>
          <div className="product__article-color">
            <span className="product__article">Артикул: {id} </span><span className="product__color">Цвет: {color}</span>
          </div>
          <div className="product__price">
            <span className="product__price-name">Цена: </span><span className="product__price-value">{price} </span><span className="product__price-uah">₴</span>
          </div>
          <div className="product__button">
            {!isOnCart
              ? <Button text="Добавить в корзину" className="btn-card" backgroundColor="#288040" onClick={this.showModalFirst} />
              : <Button text="Товар в корзине" className="btn-card" backgroundColor="#d44637" onClick={this.showModalDelete} />}

          </div>
          {this.state.modal}    
        </div>

      </>

    );
  }

  showModalFirst = () => {

    const {id} = this.props;

    this.setState({
      modal: <Modal
        header="Вы хотите добавить товар в корзину?"
        text="Нажмите ОК для добавления товара в корзину или нажмите Отмена для закрытия диалогового окна"
        actions={[
          <Button key="1" backgroundColor="#B3382C" className="btn-modal" text="ОК" onClick={()=>this.addDeleteCart(id)} />,
          <Button key="2" backgroundColor="#B3382C" className="btn-modal" text="Отмена" onClick={this.closeModal} />
        ]}
        closeButton={true}
        closeFunction={this.closeModal}

      />
    });

  }

  showModalDelete = () => {

    const {id} = this.props;

    this.setState({
      modal: <Modal
        header="Вы хотите удалить товар из корзины"
        text="Нажмите ОК для удаления товара из корзины или нажмите Отмена для закрытия диалогового окна"
        actions={[
          <Button key="1" backgroundColor="#B3382C" className="btn-modal" text="ОК" onClick={()=>this.addDeleteCart(id)} />,
          <Button key="2" backgroundColor="#B3382C" className="btn-modal" text="Отмена" onClick={this.closeModal} />
        ]}
        closeButton={true}
        closeFunction={this.closeModal}

      />
    });

  }

  addDeleteCart= (id) => {

    const {toggleToCart} = this.props;

    toggleToCart(id);  
    this.setState({
      modal: null,
    })
  }

  closeModal = (event) => {
    if (event.target === event.currentTarget) {
      this.setState({
        modal: null,
      })
    }
  }


}

Product.propTypes = {
  id: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
}

Product.defaultProps = {
  id: "Unknown",
  color: "Unknown",
  title: "Unknown",
  price: "0",
  image: "null.jpg"
}

export default Product;