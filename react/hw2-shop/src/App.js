import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ProductsWrapper from './components/ProductsWrapper/ProductsWrapper';
import axios from 'axios';


import './App.scss';
import './components/Button/Button';
import './components/Modal/Modal';

class App extends PureComponent {

  state = {
    products: [],
  }

  componentDidMount() {

    if (localStorage.getItem('products') === null) {

      axios("/products.json")
        .then(response => this.setState({ products: response.data })).then(data => (localStorage.setItem('products', JSON.stringify(this.state))));

    } else {

      const prods = JSON.parse(localStorage.getItem('products'));
      this.setState({ products: prods.products });


    }

  }

  render() {

    const { products } = this.state;


    return (
      <>
        <ProductsWrapper products={products} toggleToFavourite={this.toggleToFavourite} toggleToCart={this.toggleToCart} />
      </>
    );
  }

  toggleToFavourite = (id) => {
    const { products } = this.state;
    this.setState({ products: products.map(item => (id === item.id ? { ...item, isFavourite: !item.isFavourite } : item)) }, () => {
      localStorage.setItem('products', JSON.stringify(this.state));
    });
  }

  toggleToCart = (id) => {
    const { products } = this.state;
    this.setState({ products: products.map(item => (id === item.id ? { ...item, isOnCart: !item.isOnCart } : item)) }, () => {
      localStorage.setItem('products', JSON.stringify(this.state));
    });
  }

}

App.propTypes = {
  products: PropTypes.array,
}

App.defaultProps = {
  products: [],
}

export default App;
