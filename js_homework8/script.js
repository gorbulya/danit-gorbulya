let price = document.getElementById("priceInput");
let spanErr = document.getElementById("error");
let href = document.getElementsByTagName("a")[0];

price.addEventListener( "focus" , () => {
    price.style.borderColor = "green";
});

price.addEventListener( "blur" , () => {
    let priceValue = price.value;

    if  (priceValue < 0) {

        price.style.borderColor = "red";
        price.style.color = "";
        spanErr.style.display = "inline"
        if (document.getElementById("idPrice")){
            document.getElementById("idPrice").remove();
        }


    } else if (priceValue === "") {

        price.style.borderColor = "";
        spanErr.style.display = "none";

    } else {

        price.style.borderColor = "black";
        price.style.color = "green";
        spanErr.style.display = "none";

        if (!document.getElementById("idPrice")){

            let spanPrice = document.createElement("span");
            spanPrice.setAttribute("id", "idPrice");
            spanPrice.innerHTML = `Текущая цена: ${priceValue} `;

            let closeButton = document.createElement("a");
            closeButton.href = "##";
            closeButton.innerHTML = `(Закрыть X)`;
            closeButton.addEventListener("click", closes);

            document.body.prepend(spanPrice);
            spanPrice.append(closeButton);

        } else {

            let spanPrice = document.getElementById("idPrice");
            spanPrice.innerHTML = `Текущая цена: ${priceValue} `;
            let closeButton = document.createElement("a");
            closeButton.href = "##";
            closeButton.innerHTML = `(Закрыть X)`;
            closeButton.addEventListener("click", closes);
            spanPrice.append(closeButton);
        }



    }

});

closes = function() {
    this.parentNode.remove();
    price.value = "";
}