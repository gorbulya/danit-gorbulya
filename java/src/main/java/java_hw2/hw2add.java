package java_hw2;

import java.util.StringJoiner;
import java.util.Scanner;

public class hw2add {

    //check isInteger number
    static boolean isInt(String s)
    {
        return  (s.matches("^\\d+$"));
    }

    //check isInteger number
    static boolean isPositiveOneFive(String s){
        return (Integer.parseInt(s) >= 1) && (Integer.parseInt(s) <= 5);
    }

    //get random range
    public static int randomRange(int min, int max) {
        int delta = max - min;
        return (int) (Math.random()*(delta + 1 ) + min);
    }

    //array char to string
    public static String arrayToString(char[] a) {
        StringJoiner sj = new StringJoiner(" | ");
        for (char x: a) {
            sj.add(String.format("%c", x));
        }
        return sj.toString();
    }

    //print char Array with index
    public static void printArray(char[][] array){
        int X = array.length+1;
        int Y = array[0].length+1;

        char[][] a = new char[X][Y];

        for (int x = 0; x < X; x++) {
            for (int y = 0; y < Y; y++) {
                if (y==0) {
                    a[x][y] =  (char)(x+'0');
                } else if (x==0) {
                    a[x][y] =  (char)(y+'0');
                } else {
                    a[x][y] = array[x-1][y-1];
                }
            }
        }

        for (int i = 0; i < X; i++) {
            System.out.println(arrayToString(a[i]));
        }
    }

    //fill array of '-' char
    public static char[][] fillCleanArray(char[][] array){
        int X = array.length;
        int Y = array[0].length;

        char[][] result = new char[X][Y];
        for (int x = 0; x < X; x++) {
            for (int y = 0; y < Y; y++) {

                result[x][y] = '-';
            }
        }
        return result;
    }

    //mark the shot cell
    public static char[][] markShotCell(char[][] array, int x, int y){
        array[y-1][x-1]= '*';
        return array;
    }

    //mark the target cell
    public static char[][] markTargetCell(char[][] array, int x, int y){
        array[y-1][x-1]= 'X';
        return array;
    }

    //horizontal or vertical target
    public static boolean markHorizVert(){
        int temp = (int)(Math.random()*100);
        return temp % 2 != 0;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int X = 5;
        int Y = 5;

        int XShoot;
        int YShoot;

        String XShootStr;
        String YShootStr;

        int XTarget1 = randomRange(2, X-1);
        int YTarget1 = randomRange(2, Y-1);

        int XTarget2;
        int YTarget2;
        int XTarget3;
        int YTarget3;

        if (markHorizVert()) {
            XTarget2 = XTarget1+1;
            YTarget2 = YTarget1;
            XTarget3 = XTarget1-1;
            YTarget3 = YTarget1;
        } else {
            XTarget2 = XTarget1;
            YTarget2 = YTarget1+1;
            XTarget3 = XTarget1;
            YTarget3 = YTarget1-1;
        }

        int countTarget = 0;

        char[][] field = new char[X][Y];
        int[][] fieldVisited = new int[X][Y];

        field=fillCleanArray(field);
        printArray(field);

        do {
            System.out.print("Input your number X (horizontally): ");
            XShootStr = in.nextLine();

            while (!isInt(XShootStr)||!isPositiveOneFive(XShootStr)) {
                System.out.print("Its not a number 1-5, input your correct number X: ");
                XShootStr = in.nextLine();
            }

            XShoot = Integer.parseInt(XShootStr);

            System.out.print("Input your number Y (vertically): ");
            YShootStr = in.nextLine();

            while (!isInt(YShootStr)||!isPositiveOneFive(YShootStr)) {
                System.out.print("Its not a number 1-5, input your correct number Y: ");
                YShootStr = in.nextLine();
            }

            YShoot = Integer.parseInt(YShootStr);


            if (fieldVisited[XShoot-1][YShoot-1]==1)
            {
                System.out.println("This cell has been shot");
                printArray(field);
            } else if ((XShoot==XTarget1)&&(YShoot==YTarget1)||(XShoot==XTarget2)&&(YShoot==YTarget2)||(XShoot==XTarget3)&&(YShoot==YTarget3))
            {
                field = markTargetCell(field, XShoot, YShoot);
                printArray(field);
                countTarget = countTarget+1;

            } else {
                field = markShotCell(field, XShoot, YShoot);
                printArray(field);
            }

            fieldVisited[XShoot-1][YShoot-1]=1;

        } while (countTarget<3);

        System.out.println("You have won!");
        System.out.printf("Target number x1: %d, y1: %d, x2: %d, y2: %d, x3: %d, y3: %d%n", XTarget1, YTarget1, XTarget2, YTarget2, XTarget3, YTarget3);

        printArray(field);

        in.close();
    }

}
