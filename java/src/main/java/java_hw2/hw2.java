package java_hw2;

import java.util.StringJoiner;
import java.util.Scanner;

public class  hw2 {

    //check isInteger number
    static boolean isInt(String s)
    {
        try
        { Integer.parseInt(s); return true; }
        catch(NumberFormatException er)
        { return false; }
    }

    //check isInteger number
    static boolean isPositiveOneFive(String s){
        if ((Integer.parseInt(s)>=1)&&(Integer.parseInt(s)<=5)) {
            return true;
        } else {
            return false;
        }
    }

    //get random range
    public static int randomRange(int min, int max) {
        int delta = max - min;
        return (int) (Math.random()*(delta + 1 ) + min);
    }

    //array char to string
    public static String arrayToString(char[] a) {
        StringJoiner sj = new StringJoiner(" | ");
        for (char x: a) {
            sj.add(String.format("%c", x));
        }
        return sj.toString();
    }

    //print char Array with index
    public static void printArray(char[][] array){
        int X = array.length+1;
        int Y = array[0].length+1;

        char[][] a = new char[X][Y];

        for (int x = 0; x < X; x++) {
            for (int y = 0; y < Y; y++) {
                if (y==0) {
                    a[x][y] =  (char)(x+'0');
                } else if (x==0) {
                    a[x][y] =  (char)(y+'0');
                } else {
                    a[x][y] = array[x-1][y-1];
                }
            }
        }

        for (int i = 0; i < X; i++) {
            System.out.println(arrayToString(a[i]));
        }
    }

    //fill array of '-' char
    public static char[][] fillCleanArray(char[][] array){
        int X = array.length;
        int Y = array[0].length;

        char[][] result = new char[X][Y];
        for (int x = 0; x < X; x++) {
            for (int y = 0; y < Y; y++) {

                result[x][y] = '-';
            }
        }
        return result;
    }

    //mark the shot cell
    public static char[][] markShotCell(char[][] array, int x, int y){
        array[y-1][x-1]= '*';
        return array;
    }

    //mark the target cell
    public static char[][] markTargetCell(char[][] array, int x, int y){
        array[y-1][x-1]= 'X';
        return array;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int X = 5;
        int Y = 5;

        int XShoot;
        int YShoot;

        String XShootStr;
        String YShootStr;

        int XTarget = randomRange(1, X);
        int YTarget = randomRange(1, Y);

        char[][] field = new char[X][Y];

        field=fillCleanArray(field);
        printArray(field);

        do {
            System.out.println("Input your number X (horizontally): ");
            XShootStr = in.nextLine();

            while (!isInt(XShootStr)||!isPositiveOneFive(XShootStr)) {
                System.out.println("Its not a number 1-5, input your correct number: ");
                XShootStr = in.nextLine();
            }

            XShoot = Integer.parseInt(XShootStr);

            System.out.println("Input your number Y (vertically): ");
            YShootStr = in.nextLine();

            while (!isInt(YShootStr)||!isPositiveOneFive(YShootStr)) {
                System.out.println("Its not a number 1-5, input your correct number: ");
                YShootStr = in.nextLine();
            }

            YShoot = Integer.parseInt(YShootStr);

            field = markShotCell(field, XShoot, YShoot);
            printArray(field);

        } while ((XShoot!=XTarget)||(YShoot!=YTarget));

        System.out.println("You have won!");
        System.out.printf("Target number x: %d, y: %d%n", XShoot, YShoot);

        field = markTargetCell(field, XShoot, YShoot);
        printArray(field);

        in.close();
    }
}
