package java_hw1;

import java.util.Scanner;

public class hw1 {

    //print string
    public static void print(String str) {
        System.out.printf("%s%n", str);
    }

    //get random range
    public static int randomRange(int min, int max) {
        int delta = max - min;
        return (int) (Math.random()*(delta + 1 ) + min);
    }

    public static void main(String[] args) {

        int randomNumber = randomRange(1 ,100);
        int yourNumber;

        Scanner in = new Scanner(System.in);
        print("Input your name: ");
        String name = in.nextLine();
        print(name);

        do {
            print("Input your number: ");
            yourNumber = in.nextInt();

            if (yourNumber<randomNumber) {
                print("Your number is too small. Please, try again.");
            } else if (yourNumber>randomNumber) {
                print("Your number is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s!", name);
            }

        } while (randomNumber != yourNumber);

        in.close();
    }
}
