package java_hw1;

import java.util.Scanner;
import java.util.Arrays;

public class hw1add {

    //print string
    public static void print(String str) {
        System.out.printf("%s%n", str);
    }

    //get random range
    public static int randomRange(int min, int max) {
        int delta = max - min;
        return (int) (Math.random()*(delta + 1 ) + min);
    }

    //check isInteger number
    static boolean isInt(String s)
    {
        try
        { Integer.parseInt(s); return true; }
        catch(NumberFormatException er)
        { return false; }
    }

    //add element to array
    public static int[] addElementToArray(int[] array, int added) {
        int[] result = Arrays.copyOf(array, array.length +1);
        result[array.length] = added;
        return result;
    }

    //bubble sort array
    public static int[] sortArray(int[] array){
        int n = array.length;
        int temp;

        for(int i=0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (array[j - 1] > array[j]) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {

        int randomNumber = randomRange(1 ,100);
        String yourNumberStr;
        int yourNumber;
        int[] resArray = new int[0];
        int[] resSortArray;

        Scanner in = new Scanner(System.in);
        print("Input your name: ");
        String name = in.nextLine();
        print("Let the game begin!");

        do {
            print("Input your number: ");
            yourNumberStr = in.nextLine();

            while (!isInt(yourNumberStr)) {
                print("Its not a number, input your number: ");
                yourNumberStr = in.nextLine();
            }

            yourNumber = Integer.parseInt(yourNumberStr);

            resArray = addElementToArray(resArray, yourNumber);

            if (yourNumber<randomNumber) {
                print("Your number is too small. Please, try again.");
            } else if (yourNumber>randomNumber) {
                print("Your number is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s!\n", name);
                resSortArray = sortArray(resArray);
                System.out.printf("Your sort Array:  %s\n", Arrays.toString(resSortArray));
            }

        } while (randomNumber != yourNumber);

        in.close();
    }
}
