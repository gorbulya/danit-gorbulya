package java_hw3;

import java.util.Scanner;

public class hw3 {

    public static String TrimAndLowerCase(String s){
        return s.trim().toLowerCase();
    }

    public static void PrintDayTask(String day, String task){
        System.out.printf("Your tasks for %s: %s%n", day, task);
    }

    public static void PrintDay(String day){
        System.out.printf("Please, input new tasks for %s: ", day);
    }
    public static boolean SearchChangeReschedule(String s){
        s = TrimAndLowerCase(s);
        return (s.contains("change")) || (s.contains("reschedule"));
    }

    public static String DeleteChangeReschedule(String s){
        String s1 = s.replace("change", "");
        String s2 = s1.replace("reschedule", "");
        return s2.trim();
    }

    public static void main(String[] args) {

        String[][] scedule = new String[7][2];

        scedule[0][0] = "sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "tuesday";
        scedule[2][1] = "wash the car, cook dinner";
        scedule[3][0] = "wednesday";
        scedule[3][1] = "buy groceries, call mom";
        scedule[4][0] = "thursday";
        scedule[4][1] = "pay the bills, finish the project";
        scedule[5][0] = "friday";
        scedule[5][1] = "to meet friends";
        scedule[6][0] = "saturday";
        scedule[6][1] = "go for a picnic";

        Scanner scan = new Scanner(System.in);

        boolean exit = false;

        while (!exit) {

            System.out.print("Please, input the '%day' of the week / command 'change %day' or 'reschedule %day' / 'exit' for exit: ");
            String str = TrimAndLowerCase(scan.nextLine());

            if (SearchChangeReschedule(str)){

                str = DeleteChangeReschedule(str);

                switch (str){
                    case "sunday":
                        PrintDay(scedule[0][0]);
                        scedule[0][1] = scan.nextLine();
                        break;
                    case "monday":
                        PrintDay(scedule[1][0]);
                        scedule[1][1] = scan.nextLine();
                        break;
                    case "tuesday":
                        PrintDay(scedule[2][0]);
                        scedule[2][1] = scan.nextLine();
                        break;
                    case "wednesday":
                        PrintDay(scedule[3][0]);
                        scedule[3][1] = scan.nextLine();
                        break;
                    case "thursday":
                        PrintDay(scedule[4][0]);
                        scedule[4][1] = scan.nextLine();
                        break;
                    case "friday":
                        PrintDay(scedule[5][0]);
                        scedule[5][1] = scan.nextLine();
                        break;
                    case "saturday":
                        PrintDay(scedule[6][0]);
                        scedule[6][1] = scan.nextLine();
                        break;
                    case "exit":
                        System.out.println("Exit the program!!");
                        exit =true;
                        break;
                    default: System.out.println("Sorry, I don't understand you, please try again.");
                }


            } else {
                switch (str){
                    case "sunday":
                        PrintDayTask(scedule[0][0], scedule[0][1]);
                        break;
                    case "monday":
                        PrintDayTask(scedule[1][0], scedule[1][1]);
                        break;
                    case "tuesday":
                        PrintDayTask(scedule[2][0], scedule[2][1]);
                        break;
                    case "wednesday":
                        PrintDayTask(scedule[3][0], scedule[3][1]);
                        break;
                    case "thursday":
                        PrintDayTask(scedule[4][0], scedule[4][1]);
                        break;
                    case "friday":
                        PrintDayTask(scedule[5][0], scedule[5][1]);
                        break;
                    case "saturday":
                        PrintDayTask(scedule[6][0], scedule[6][1]);
                        break;
                    case "exit":
                        System.out.println ("Exit the program!!");
                        exit =true;
                        break;
                    default: System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
        }

        scan.close();
    }

}
