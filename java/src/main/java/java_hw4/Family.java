package java_hw4;

import java.util.Arrays;
import java.util.Objects;

public class Family {

    static {
        System.out.printf("Загружается новый класс : %s \n" , Family.class.getSimpleName());
    }

    {
        System.out.printf("Cоздается новый объект : %s \n" , Family.class.getSimpleName());
    }

    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.children = new Human[]{};
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human human){
        children = Arrays.copyOf(children, children.length + 1);
        children[children.length - 1] = human;
        human.setFamily(this);
    }

    public int countFamily(){
        return 2 + children.length;

    }

    public void deleteChild(Human child) {
        if (children.length > 0 && Arrays.asList(children).indexOf(child) != -1) {
            Human[] result = new Human[children.length-1];
            int j = 0;
            for (Human aChildren : children) {
                if (aChildren.hashCode() == child.hashCode()) {
                    if (aChildren.equals(child)) {
                        continue;
                    }
                }
                result[j] = aChildren;
                j++;
            }
            children = result;
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }



    @Override
    public String toString() {
        return "Family{" + "\n" +
                "mother=" + mother + "\n" +
                ", father=" + father + "\n" +
                ", children=" + Arrays.toString(children) + "\n" +
                ", pet=" + pet + "\n" + "}";
    }


}
