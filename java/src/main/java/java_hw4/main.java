package java_hw4;

public class main {

    public static void main(String[] args) {

        Family family = new Family(new Human("Artem", "Ivanov", 35 ), new Human("Galina", "Ivanova", 36, 81));

        Human child = new Human("Maxim", "Ivanov", 16);
        Human child2 = new Human();
        Human child3 = new Human("Petya", "Ivanov", 5, 30);
        Human child4 = new Human("MAria", "Ivanova", 5, 80, new String[][]{{"понедельник","пора в школу"}, {"вторник","пора на кружок"}});

        Pet dog = new Pet("dog", "Rex", 9, 30, new String[] {"есть", "спать", "играть"});

        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        family.setPet(dog);

        System.out.println(family);
        System.out.print("Количество человек в семье: ");
        System.out.println(family.countFamily());

        family.deleteChild(child2);

        System.out.println(family);
        System.out.print("Количество человек в семье: ");
        System.out.println(family.countFamily());

        child2.feedPet(true);
        child2.greetPet();
        child2.describePet();

        dog.eat();
        dog.respond();
        dog.foul();

        family.getMother().feedPet(true);
        family.getMother().feedPet(false);
        family.getMother().feedPet(false);
        family.getMother().feedPet(false);

    }

}
