// 1. Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// 2. Сделайте геттеры и сеттеры для этих свойств.
// 3. Сделайте класс Programmer, который будет наследоваться от класса Employee, у которого будет свойство lang (список языков)
// 4. Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary умноженное на 3.
// 5. Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.


class Employee {
    constructor ({name, age, salary}) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(val){
        this._name = val;
    }

    get name(){
        return this._name;
    }

    set age(val){
        this._age = val;
    }

    get age(){
        return this._age;
    }

    set salary(val){
        this._salary = val;
    }

    get salary(){
        return this._salary;
    }
}


const programmer1 = new Employee ({
    name:'Artem',
    age: 36,
    salary: 2000
});

console.log(programmer1);
console.log(programmer1.salary);
programmer1.salary = 3000;
console.log(programmer1.salary);


class Programmer extends Employee{

    constructor({lang, ...baseParams}) {
        super(baseParams);
        this._lang = lang;
    }

    get salary(){
        return this._salary * 3;
    }

    set salary(val){
        this._salary = val;
    }

}

const programmer2 = new Programmer({
    lang: 'php',
    name:'Ivan',
    age: 20,
    salary: 2000
});

const programmer3 = new Programmer({
    lang: 'js',
    name:'Vitaly',
    age: 30,
    salary: 3000
});

const programmer4 = new Programmer({
    lang: 'c++',
    name:'Sergy',
    age: 40,
    salary: 4000
});


console.log(programmer2.salary);
programmer2.salary = 5000;
console.log(programmer2.salary);

console.log(programmer2);
console.log(programmer3);
console.log(programmer4);



