document.addEventListener("DOMContentLoaded", ready);

function ready() {

    function createTable() {

        let table = document.createElement('table');

        for (let i = 0; i < 30; i++) {
            let tr = document.createElement('tr');
            table.appendChild(tr);
            for (let j = 0; j < 30; j++) {
                let td = document.createElement('td');
                tr.appendChild(td);
            }
        }

        document.body.appendChild(table);

    }

    function inversCell(){
        let tableInHtml = document.getElementsByTagName("table")[0];
        tableInHtml.addEventListener("click", (event) => {
            if (event.target.tagName === "TD") {
                event.target.classList.toggle("black");
                event.stopPropagation();
            }

        })
    }

    function inversTable(){
        let bodyHtml = document.body;
        bodyHtml.addEventListener("click", (event) => {
            if (event.target.tagName === "BODY") {
                let tableInHtml = document.getElementsByTagName("table")[0];
                tableInHtml.classList.toggle("reverse");
                event.stopPropagation();
            }
        })

    }


    createTable();
    inversCell();
    inversTable();


}