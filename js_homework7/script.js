// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// Создать функцию, которая будет принимать на вход массив.
// Каждый из элементов массива вывести на страницу в виде пункта списка
// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//
// Примеры массивов, которые можно выводить на экран:
// ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
// ['1', '2', '3', 'sea', 'user', 23]
//
// Можно взять любой другой массив.
//
// Необязательное задание продвинутой сложности:
//
// Очистить страницу через 10 секунд. Показывать таймер обратного отсчета (только секунды) перед очищением страницы.
// Если внутри массива одним из элементов будет еще один массив или объект, выводить его как вложенный список.

let addListNumberTwo = ['1', '2', '3',['hello',['red','blue','green'],'Kiev','Kharkiv','Odessa','Lviv'], '5', '6' ];

//функция формирования html кода из массива
function arrayToList(arr) {
    let mapArray = `<ul> ${arr.map( function(num){
        if (Array.isArray(num)) {
            return ("<li>" +  arrayToList(num) + "</li>");             
        } else {
            return (`<li> ${num}</li>`);
        }
    }
        )} </ul>`;

        return mapArray;

}

//функция вставки кода в страницу
function addToHtml (arr) {
    document.getElementById('container').innerHTML = arrayToList(arr).split(',').join('');
}


//функция обратного счетчика
let sec = 10;
function timeSec() {
    if (sec < 1) {
        document.body.innerHTML = '';
    }
    else {
        document.getElementById('timer').innerHTML = `${sec}`;
        setTimeout(timeSec, 1000);
        sec--;
    }
}

addToHtml(addListNumberTwo);
timeSec();