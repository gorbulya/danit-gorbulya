document.addEventListener("DOMContentLoaded", ready);

function ready() {

    let links = document.querySelectorAll(".tabs li");
    let content = document.querySelectorAll(".tabs-content li");

    for(let i=0; i <links.length; i++) {
        (function(i) {
            let link = links[i];
            link.onclick = function() {
                for(let j=0; j <content.length; j++) {

                    let opacity = window.getComputedStyle(content[j]).opacity;
                    if(opacity == "1") {
                        content[j].style.opacity = "0";

                    }
                }

                for (let i=0; i <links.length; i++){
                    links[i].classList.remove("active");
                }

                content[i].style.opacity = "1";
                link.classList.add("active");
            }
        })(i);
    }

}

