package family;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;



public class HumanTest {

    Human human = new Human("MAria", "Ivanova", 5, 80, new String[][]{{DayOfWeek.MONDAY.name(),"пора в школу"}, {DayOfWeek.THURSDAY.name(),"пора на кружок"}});

    public String getExpectedToString(Human human) {
        return "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", schedule=" + Arrays.deepToString(human.getSchedule()) + "}";
    }

    @Test
    public void testToString(){
        assertEquals(human.toString(), getExpectedToString(human));
    }

}