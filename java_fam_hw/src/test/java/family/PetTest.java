package family;


import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;



public class PetTest {

    Pet pet = new Pet(Species.DOG, "Rex", 9, 30, new String[] {"есть", "спать", "играть"});

    public String getExpectedToString(Pet pet) {
        return pet.getSpecies() + "{CanFly="+ pet.getSpecies().getCanFly() + ", NumberOfLegs=" + pet.getSpecies().getNumberOfLegs() + ", HasFur=" + pet.getSpecies().getHasFur() + "}" + "{nickname='" + pet.getNickname() + "', age=" + pet.getAge() + ", trickLevel=" +
                pet.getTrickLevel() + ", habits=" + Arrays.toString(pet.getHabits()) + "}";
    }

    @Test
    public void testToString(){
        assertEquals(pet.toString(), getExpectedToString(pet));
    }


}
