package family;

public enum Species {
    CAT (false, 4, true),
    DOG (false, 4, true),
    FISH (false, 0, false);

    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur){
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    public boolean getCanFly(){
        return canFly;
    }

    public void setCanFly(boolean canFly){
        this.canFly=canFly;
    }

    public int getNumberOfLegs(){
        return numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs){
        this.numberOfLegs=numberOfLegs;
    }

    public boolean getHasFur(){
        return hasFur;
    }

    public void setHasFur(boolean hasFur){
        this.hasFur = hasFur;
    }

}
