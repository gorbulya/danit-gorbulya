//Задание
//Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
//Технические требования:
//
//Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//Создать метод getAge() который будет возвращать сколько пользователю лет.
//Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
//Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser() {

    let firstN = prompt("Input your firstName", "Jesus");

    while ( firstN === "" || firstN === null){
        firstN = prompt("Its not name - Input your firstName");
    }

    let lastN = prompt("Input your lastName", "God");

    while ( lastN === "" || lastN === null){
        lastN = prompt("Its not lastName - Input your lastName");
    }

    let birthD = prompt("Input your birthday dd.mm.yyyy", "11.11.1984");

    while ( birthD === "" || birthD === null){
        birthD = prompt("Its not birthday - Input your birthday dd.mm.yyyy");
    }



    let newUser = {
        firstName: firstN,
        lastName: lastN,
        birthDay: birthD,

        set setFirstName(value){
            this.firstName = value;

        },
        set setLastName(value){
            this.lastName = value;

        },

        getLogin () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },

        getAge(){
            let ageString = this.birthDay.split(".");
            let yearUser = ageString[2];
            let monthUser = ageString[1];
            let dayUser = ageString[0];
            let today = new Date();
            let newYear = today.getFullYear();
            let newMonth = today.getMonth()+1;
            let newDay = today.getDate();

            if (monthUser < newMonth) {
                return newYear-yearUser;
            } else if (monthUser > newMonth){
                return newYear-yearUser-1;
            } else if (monthUser == newMonth){
                if (dayUser >  newDay) {
                    return newYear-yearUser-1;
                } else {
                    return newYear-yearUser;
                }
            }
        },

        getPassword(){
            let ageString = this.birthDay.split(".");
            let yearUser = ageString[2];

            return this.firstName.charAt(0).toLocaleUpperCase() + this.lastName.toLowerCase() + yearUser;
        }

    };

return newUser;

}


let newUser = createNewUser();
let login = newUser.getLogin();
console.log(newUser);
console.log("Логин пользователя: " + login);
console.log("Пароль пользователя: " + newUser.getPassword());
console.log("Полных лет пользователю: " + newUser.getAge());