document.addEventListener("DOMContentLoaded", ready);

function ready() {

    function getRandomColor() {
        const r = Math.floor(Math.random() * 255);
        const g = Math.floor(Math.random() * 255);
        const b = Math.floor(Math.random() * 255);

        return `rgb(${r}, ${g}, ${b})`;
    }


    const startButton = document.getElementById("start-button");
    let content = null;

    startButton.addEventListener("click",  () => {

        if(!content){
            startButton.insertAdjacentHTML("afterend", `
             <br/><input type="number" id="diametr" placeholder="Введите диаметр круга"><br/>
            <div id="circle-content"></div>
            `);

            content = document.getElementById("circle-content");

            content.addEventListener("click", (event) => {
                if(event.target.classList.contains("circle-one")){
                    event.target.remove();
                }
            })

        } else {

            const diameterCircle = document.getElementById("diametr").value;
            let addCode = new DocumentFragment();

            for(let i = 0; i < 100; i++){
                const circleOne = document.createElement("div");
                let randomColor = getRandomColor();
                circleOne.classList.add("circle-one");
                circleOne.style.cssText = `width: ${diameterCircle}px; height: ${diameterCircle}px; background-color: ${randomColor}`;
                addCode.append(circleOne);
            }

            content.append(addCode);
        }
    });


}