// Задание
// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//При вызове функция должна спросить у вызывающего имя и фамилию.
//Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
//
//
//Необязательное задание продвинутой сложности:
//
//Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

function createNewUser() {

    let firstN = prompt("Input your firstName", "Jesus");

    while ( firstN === "" || firstN === null){
        firstN = prompt("Its not name - Input your firstName");
    }

    let lastN = prompt("Input your lastName", "God");

    while ( lastN === "" || lastN === null){
        lastN = prompt("Its not lastName - Input your lastName");
    }

    let newUser = {
        firstName: firstN,
        lastName: lastN,

        set setfirstName(value){
            this.firstName = value;

        },
        set setlastName(value){
            this.lastName = value;

        },

        getLogin () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        }

    };

return newUser;

}


let newUser = createNewUser();
newUser.setfirstName = "Artem";
newUser.setlastName = "Gorbulya";
let login = newUser.getLogin();
console.log(newUser);
console.log(login);