
document.addEventListener("DOMContentLoaded", ready);

function ready() {

    let imgContainer = document.getElementById('imgWrapper');

    for (let i = 0; i < imgContainer.children.length; i++){
        imgContainer.children[i].dataset.index = i;
        if(i !== 0)
            imgContainer.children[i].hidden = true;
    }

    let stopButton = document.createElement('button');
    let continueButton = document.createElement('button');
    let timer = document.createElement('p');

    stopButton.innerText = "Прекратить";
    continueButton.innerText = "Возобновить";

    document.body.appendChild(timer);
    document.body.appendChild(stopButton);
    document.body.appendChild(continueButton);

    let intervalId, timerIntervalId;

    function startInterval(){
        timer.innerText = "10.00";
        timerIntervalId = setInterval(() => {
            timer.innerText = (timer.innerText - 0.01).toFixed(2);
        }, 10);

        intervalId = setInterval(() => {
            timer.innerText = "10.00";
            let currentEl = imgContainer.querySelector('img:not([hidden])');
            currentEl.hidden = true;
            if (currentEl === imgContainer.lastElementChild){
                imgContainer.firstElementChild.hidden = false;
            } else {
                imgContainer.children[+currentEl.dataset.index + 1].hidden = false;
            }
        }, 10000)
    }

    stopButton.onclick = () => {
        clearInterval(timerIntervalId);
        clearInterval(intervalId);
    };
    continueButton.onclick = () => {
        startInterval();
    };

    startInterval();


}