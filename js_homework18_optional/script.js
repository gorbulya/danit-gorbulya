// Данное задание не обязательно для выполнения
//
// Задание
// Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
// В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.


let userObject = {
    name: "Jesus",
    lastName: "God",
    address:{
        street: "Main street",
        house: {
            floor: 3,
            number: 14
        }


    }
};

function cloneObject(userObject){
    let newObject = {};
    for (let key in userObject){
        if (typeof userObject[key] === "object"){
            newObject[key] = cloneObject(userObject[key]);
        } else {
            newObject[key] = userObject[key];
        }
    }
    return newObject;
}

let clonedObject = cloneObject(userObject);

console.log(clonedObject);