document.addEventListener("DOMContentLoaded", ready);

function ready() {

    let inputFirst = document.getElementById("first-input");
    let inputSecond = document.getElementById("second-input");
    let buttonSend = document.getElementsByClassName("btn")[0];

    let spanError = document.createElement("span");
    spanError.innerHTML = "Нужно ввести одинаковые значения";
    spanError.style.color = "red";


    inputFirst.nextElementSibling.addEventListener("click", () => {
        changeInput("first-input")
    });
    inputSecond.nextElementSibling.addEventListener("click", () => {
        changeInput("second-input")
    });
    buttonSend.addEventListener("click", () => {
        checkPass();
        event.preventDefault();
    });


    function changeInput(id) {
        let input = document.getElementById(id);
        input.nextElementSibling.classList.toggle("fa-eye-slash");
        input.nextElementSibling.classList.add("fa-eye");

        input.removeAttribute("type");

        if (input.nextElementSibling.classList.contains("fa-eye-slash")) {
            input.setAttribute("type", "password");
        } else {
            input.setAttribute("type", "text");
        }
    }

    function checkPass() {
        let pass1 = inputFirst.value;
        let pass2 = inputSecond.value;

        spanError.remove();

        if (pass1 === pass2 && pass1 !== "") {
            alert("You are welcome");
        } else if (pass1 === pass2 && pass1 === "") {
            alert("Вы не ввели пароль");
        } else {
            document.body.append(spanError);
        }

    }

}