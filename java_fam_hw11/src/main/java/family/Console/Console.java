package family.Console;

import family.DAO.CollectionFamilyDao;
import family.DAO.FamilyDao;
import family.Exeption.FamilyOverflowException;
import family.FamilyController;
import family.FamilyService;
import family.Humans.Family;
import family.Humans.Human;
import family.Humans.Man;
import family.Humans.Woman;
import family.Pets.Dog;
import family.Pets.DomesticCat;
import family.Pets.Pet;
import family.Pets.RoboCat;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Console {

    private static final Scanner SCANNER = new Scanner(System.in);
    private static final String REG_STR = "\\D+";
    private static final String REG_NUM = "([0-9])+";

    private FamilyController familyController = null;

    private String cmdCreateData = "-1. Fill with test data.\n";
    private String cmdDisplayAll = "-2. Display all list of families.\n";
    private String cmdFamilyMore = "-3. Display a list of families where the number of people is more than the specified.\n";
    private String cmdFamilyLess = "-4. Display a list of families where the number of people is less than the specified.\n";
    private String cmdFamilyCount = "-5. Count the number of families where the number of members is.\n";
    private String cmdCreateNewFamily = "-6. Create new family.\n";
    private String cmdDeleteFamily = "-7. Delete family by family index in the general list.\n";
    private String cmdEditFamily = "-8. Edit family by family index in the general list.\n";
    private String cmdRemoveChild = "-9. Remove all children over age.";

    public static FamilyService createFamily() {
        Human child1 = new Man("Vlad", "Horbulia", "01/03/2016");
        Human child2 = new Man("Nick", "Ivanov", "01/03/2011");
        Human child3 = new Man("Den", "Horbulia", "01/03/2011");
        Human woman = new Woman("Ksenia", "Horbulia" , "01/03/1991", 50, null);
        Human man = new Man("Artem", "Horbulia", "01/03/1996", 50, null);

        Family family = new Family(woman, man);
        Set<Pet> pets = new HashSet<>();

        Set<String> habits = new HashSet<>();
        habits.add("есть");
        habits.add("спать");
        habits.add("играть");

        Pet pet = new Dog( "Rex", 9, 30, habits);
        Pet pet1 = new DomesticCat("Izi", 5, 50, habits);
        Pet pet2 = new RoboCat("Robo", 1, 30, habits);

        pets.add(pet);
        pets.add(pet1);
        pets.add(pet2);
        family.addChild(child1);
        family.addChild(child3);
        family.setPet(pets);

        Human woman1 = new Woman("Kate", "Ivanova", "01/03/1991", 50, null);
        Human man1 = new Man("Bob", "Ivanov", "01/03/1991", 50, null);
        Family family1 = new Family(woman1, man1);
        family1.addChild(child2);

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);

        FamilyService familyService = new FamilyService(familyDao);
        return familyService;
    }

    public void consoleRun() {
        String command = "";

        while (!command.equalsIgnoreCase("exit")) {
            printCommand();
            System.out.print("\nPlease, enter number of command: ");

            try {
                switch (command = SCANNER.next().toLowerCase()) {

                    case "1":
                        createData();
                        break;

                    case "2":
                        displayAllFamilies();
                        break;

                    case "3":
                        displayFamiliesMoreThen();
                        break;

                    case "4":
                        displayFamiliesLessThen();
                        break;

                    case "5":
                        familyCountWithMember();
                        break;

                    case "6":
                        createNewFamily();
                        break;

                    case "7":
                        deleteFamily();
                        break;

                    case "8":
                        editFamily();
                        break;

                    case "9":
                        removeChildOver();
                        break;

                    case "exit":
                        break;

                    default: throw new RuntimeException();
                }
            } catch (NullPointerException e) {
                System.out.println("Please, create data families. Enter number 1");
            } catch (NumberFormatException e) {
                System.out.println("Enter number");
            } catch (FamilyOverflowException e) {
                System.out.println(e.getMessage());
            } catch (RuntimeException e) {
                System.out.println("Invalid operation");
            }
        }
    }

    private void printCommand() {
        System.out.println("\n" + cmdCreateData + cmdDisplayAll + cmdFamilyMore + cmdFamilyLess + cmdFamilyCount +
                cmdCreateNewFamily + cmdDeleteFamily + cmdEditFamily + cmdRemoveChild);
    }

    private void createData() {
        System.out.println("-1. Fill with test data:\n");
        familyController = new FamilyController(createFamily());
        System.out.println("test data added successfully");
    }

    private void displayAllFamilies() {
        System.out.println(cmdDisplayAll);
        familyController.displayAllFamilies();
    }

    private void displayFamiliesMoreThen() {
        System.out.println(cmdFamilyMore);
        System.out.print("\t-Enter your number: ");
        int number = SCANNER.nextInt();
        System.out.println();
        familyController.getFamiliesBiggerThan(number);
    }

    private void displayFamiliesLessThen() {
        System.out.println(cmdFamilyLess);
        System.out.print("\t-Enter your number: ");
        int number1 = SCANNER.nextInt();
        System.out.println();
        familyController.getFamiliesLessThan(number1);
    }

    private void familyCountWithMember() {
        System.out.println(cmdFamilyCount);
        System.out.print("\t-Enter your number: ");
        int number2 = SCANNER.nextInt();
        System.out.println();
        familyController.countFamiliesWithMemberNumber(number2);
    }

    private void createNewFamily() {
        System.out.println(cmdCreateNewFamily);
        Human mother = createWoman();
        Human father = createMan();
        familyController.createNewFamily(mother, father);
        System.out.println("Congratulation, your family added to main list of families!");
    }

    private Human createWoman() {
        System.out.print("\t-What is woman name? ");
        String womanName = validEnter(REG_STR);
        System.out.print("\t-What is woman surname? ");
        String womanSurname = validEnter(REG_STR);
        System.out.print("\t-What is woman year of birth? ");
        int womanYearBirth = Integer.parseInt(validEnter(REG_NUM));
        System.out.print("\t-What is woman month of birth? ");
        int womanMonthBirth = Integer.parseInt(validEnter(REG_NUM));
        System.out.print("\t-What is woman day of birth? ");
        int womanDayBirth = Integer.parseInt(validEnter(REG_NUM));
        System.out.print("\t-What is woman iq? ");
        int womanIq = Integer.parseInt(validEnter(REG_NUM));

        String resultBirthDate = womanDayBirth + "/" + womanMonthBirth + "/" + womanYearBirth;

        Human woman = new Woman(womanName, womanSurname, resultBirthDate, womanIq, null);
        return woman;
    }

    private Human createMan() {
        System.out.print("\t-What is man name? ");
        String manName = validEnter(REG_STR);
        System.out.print("\t-What is man surname? ");
        String manSurname = validEnter(REG_STR);
        System.out.print("\t-What is man year of birth? ");
        int manYearBirth = Integer.parseInt(validEnter(REG_NUM));
        System.out.print("\t-What is man month of birth? ");
        int manMonthBirth = Integer.parseInt(validEnter(REG_NUM));
        System.out.print("\t-What is man day of birth? ");
        int manDayBirth = Integer.parseInt(validEnter(REG_NUM));
        System.out.print("\t-What is man iq? ");
        int manIq = Integer.parseInt(validEnter(REG_NUM));

        String resultBirthDate = manDayBirth + "/" + manMonthBirth + "/" + manYearBirth;

        Human man = new Man(manName, manSurname, resultBirthDate, manIq , null);
        return man;
    }

    private String validEnter(String regex){
        String string = SCANNER.next();
        while(!string.matches(regex)){
            System.out.print("\nYour data not correct: " + string);
            System.out.print( "\n\t- Enter valid data: ");
            string = SCANNER.next();
        }
        return string;
    }

    private void deleteFamily() {
        System.out.println(cmdDeleteFamily);
        System.out.print("\t-Enter your number: ");
        int index = SCANNER.nextInt();
        if (familyController.count() < index) {
            System.out.println("Nothing to delete");
        } else {
            familyController.deleteFamilyByIndex(index);
            System.out.println("Family by index " + index + " has been deleted.");
        }
    }

    private void editFamily() {
        System.out.println(cmdEditFamily);
        System.out.println("\t-1. Add child to family\n" + "\t-2. Adopt child\n" + "\t-3. Return to the main menu");
        System.out.print("Please, enter number of command: ");
        int number3 = Integer.parseInt(validEnter("([1-3])+"));

        if (number3 == 1) {
            System.out.print("\t-Enter family number(ID): ");
            int familyId = SCANNER.nextInt();
            while (familyController.count() < familyId) {
                System.out.print("Enter valid ID family: ");
                familyId = SCANNER.nextInt();
            }
            System.out.print("\t-Enter boy name: ");
            String boyName = validEnter(REG_STR);
            System.out.print("\t-Enter girl name: ");
            String girlName = validEnter(REG_STR);
            Family familyById = familyController.getFamilyById(familyId);
            familyController.bornChild(familyById, boyName, girlName);
            System.out.println("Child added to family!");
        }

        if (number3 == 2) {
            System.out.print("\t-Enter family number(ID): ");
            int familyId = SCANNER.nextInt();
            while (familyController.count() < familyId) {
                System.out.print("Enter valid ID family: ");
                familyId = SCANNER.nextInt();
            }
            System.out.print("\t-Enter all information about child. If you want to adopt boy press 1 or girl pres 2: ");
            int chosenNumber = Integer.parseInt(validEnter("([1-2])+"));

            Family familyById = familyController.getFamilyById(familyId);

            if (chosenNumber == 1) {
                familyController.adoptChild(familyById, createMan());
            } else if (chosenNumber == 2) {
                familyController.adoptChild(familyById, createWoman());
            } else {
                System.out.println("Enter correct number!");
            }
            System.out.println("Congratulation, you adopted child to your family!");
        }
    }

    private void removeChildOver() {
        System.out.println(cmdRemoveChild);
        System.out.print("\t-Enter your age: ");
        int age = SCANNER.nextInt();
        try {
            familyController.deleteAllChildrenOlderThen(age);
        } catch (Error e) {
            e.printStackTrace();
        }
        }
}


