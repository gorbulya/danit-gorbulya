package family.Pets;

import family.Enum.Species;
import family.Pets.Pet;

import java.util.Set;

public class Fish extends Pet {

    {
        super.setSpecies(Species.FISH);
    }

    public Fish() {
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("My name is " + this.getNickname());
    }
}

