package family;

import family.Console.Console;
import family.DAO.CollectionFamilyDao;
import family.DAO.FamilyDao;
import family.Enum.DayOfWeek;
import family.Humans.Family;
import family.Humans.Human;
import family.Humans.Man;
import family.Humans.Woman;
import family.Pets.Dog;
import family.Pets.DomesticCat;
import family.Pets.Pet;
import family.Pets.RoboCat;

import java.text.ParseException;
import java.util.*;

public class main {

//    private static final Scanner SCANNER = new Scanner(System.in);
//
//    public static FamilyService createFamily() {
//        Human child1 = new Man("Vlad", "Horbulia", "01/03/2016");
//        Human child2 = new Man("Nick", "Ivanov", "01/03/2011");
//        Human child3 = new Man("Den", "Horbulia", "01/03/2011");
//        Human woman = new Woman("Ksenia", "Horbulia" , "01/03/1991", 50, null);
//        Human man = new Man("Artem", "Horbulia", "01/03/1996", 50, null);
//
//        Family family = new Family(woman, man);
//        Set<Pet> pets = new HashSet<>();
//
//        Set<String> habits = new HashSet<>();
//           habits.add("есть");
//           habits.add("спать");
//           habits.add("играть");
//
//        Pet pet = new Dog( "Rex", 9, 30, habits);
//        Pet pet1 = new DomesticCat("Izi", 5, 50, habits);
//        Pet pet2 = new RoboCat("Robo", 1, 30, habits);
//
//        pets.add(pet);
//        pets.add(pet1);
//        pets.add(pet2);
//        family.addChild(child1);
//        family.addChild(child3);
//        family.setPet(pets);
//
//        Human woman1 = new Woman("Kate", "Ivanova", "01/03/1991", 50, null);
//        Human man1 = new Man("Bob", "Ivanov", "01/03/1991", 50, null);
//        Family family1 = new Family(woman1, man1);
//        family1.addChild(child2);
//
//        FamilyDao familyDao = new CollectionFamilyDao();
//        familyDao.saveFamily(family);
//        familyDao.saveFamily(family1);
//
//        FamilyService familyService = new FamilyService(familyDao);
//        return familyService;
//    }
//
//    public static void printCommand() {
//        System.out.println(
//                "\n-1. Fill with test data.\n" +
//                        "-2. Display all list of families.\n" +
//                        "-3. Display a list of families where the number of people is more than the specified.\n" +
//                        "-4. Display a list of families where the number of people is less than the specified.\n" +
//                        "-5. Count the number of families where the number of members is.\n" +
//                        "-6. Create new family.\n" +
//                        "-7. Delete family by family index in the general list.\n" +
//                        "-8. Edit family by family index in the general list.\n" +
//                        "-9. Remove all children over age."
//        );
//    }
//
//    public static void warningCreateTest() {
//        System.out.println("Create test data family. Enter number 1");
//    }
//
//    public static Human createWoman() {
//        System.out.print("\t-What is woman name? ");
//        String womanName = SCANNER.next();
//        System.out.print("\t-What is woman surname? ");
//        String womanSurname = SCANNER.next();
//        System.out.print("\t-What is woman year of birth? ");
//        int womanYearBirth = SCANNER.nextInt();
//        System.out.print("\t-What is woman month of birth? ");
//        int womanMonthBirth = SCANNER.nextInt();
//        System.out.print("\t-What is woman day of birth? ");
//        int womanDayBirth = SCANNER.nextInt();
//        System.out.print("\t-What is woman iq? ");
//        int womanIq = SCANNER.nextInt();
//
//        String resultBirthDate = womanDayBirth + "/" + womanMonthBirth + "/" + womanYearBirth;
//
//        Human woman = new Woman(womanName, womanSurname, resultBirthDate, womanIq, null);
//        return woman;
//    }
//
//    public static Human createMan() {
//        System.out.print("\t-What is man name? ");
//        String manName = SCANNER.next();
//        System.out.print("\t-What is man surname? ");
//        String manSurname = SCANNER.next();
//        System.out.print("\t-What is man year of birth? ");
//        int manYearBirth = SCANNER.nextInt();
//        System.out.print("\t-What is man month of birth? ");
//        int manMonthBirth = SCANNER.nextInt();
//        System.out.print("\t-What is man day of birth? ");
//        int manDayBirth = SCANNER.nextInt();
//        System.out.print("\t-What is man iq? ");
//        int manIq = SCANNER.nextInt();
//
//        String resultBirthDate = manDayBirth + "/" + manMonthBirth + "/" + manYearBirth;
//
//        Human woman = new Woman(manName, manSurname, resultBirthDate, manIq, null);
//        return woman;
//    }






    public static void main(String[] args) {

//        String command;
//        FamilyController familyController = null;
//
//
//
//        do {
//            printCommand();
//
//            System.out.print("\nPlease, enter number of command: ");
//            switch (command = SCANNER.next()) {
//                case "1":
//                    System.out.println("-1. Fill with test data:\n");
//                    familyController = new FamilyController(createFamily());
//                    System.out.println("test data added successfully");
//                    break;
//
//                case "2":
//                    System.out.println("-2. Display all list of families:\n");
//                    if (familyController != null) {
//                        familyController.displayAllFamilies();
//                    } else warningCreateTest();
//                    break;
//
//                case "3":
//                    System.out.println("-3. Display a list of families where the number of people is more than the specified:");
//                    System.out.print("\t-Enter your number: ");
//                    int number = SCANNER.nextInt();
//                    System.out.println();
//                    if (familyController != null) {
//                        familyController.getFamiliesBiggerThan(number);
//                    } else warningCreateTest();
//                    break;
//
//                case "4":
//                    System.out.println("-4. Display a list of families where the number of people is less than the specified:");
//                    System.out.print("\t-Enter your number: ");
//                    int number1 = SCANNER.nextInt();
//                    System.out.println();
//                    if (familyController != null) {
//                        familyController.getFamiliesLessThan(number1);
//                    } else warningCreateTest();
//                    break;
//
//                case "5":
//                    System.out.println("-5. Count the number of families where the number of members is:");
//                    System.out.print("\t-Enter your number: ");
//                    int number2 = SCANNER.nextInt();
//                    System.out.println();
//                    if (familyController != null) {
//                        familyController.countFamiliesWithMemberNumber(number2);
//                    } else warningCreateTest();
//                    break;
//
//
//                case "6":
//                    System.out.println("-6. Create new family:");
//                    Human mother = createWoman();
//                    Human father = createMan();
//                    if (familyController != null) {
//                        familyController.createNewFamily(mother, father);
//                    }
//                    System.out.println("Congratulation, your family added to main list of families!");
//                    break;
//
//                case "7":
//                    System.out.println("-7. Delete family by family index in the general list:");
//                    System.out.print("\t-Enter your number: ");
//                    int index = SCANNER.nextInt();
//                    if (familyController != null) {
//                        familyController.deleteFamilyByIndex(index);
//                    }
//                    System.out.println("Family by index " + index + " has been deleted.");
//                    break;
//
//                case "8":
//                    System.out.println("-8. Edit family by family index in the general list: ");
//                    System.out.println("\t-1. Add child to family\n" + "\t-2. Adopt child\n" + "\t-3. Return to the main menu");
//                    System.out.print("Please, enter number of command: ");
//                    int number3 = SCANNER.nextInt();
//
//                    if (number3 == 1) {
//                        System.out.print("\t-Enter family number(ID): ");
//                        int familyId = SCANNER.nextInt();
//                        System.out.print("\t-Enter boy name: ");
//                        String boyName = SCANNER.next();
//                        System.out.print("\t-Enter girl name: ");
//                        String girlName = SCANNER.next();
//
//                        if (familyController != null) {
//                            Family familyById = familyController.getFamilyById(familyId);
//                            familyController.bornChild(familyById, boyName, girlName);
//                        }
//                        System.out.println("Child added to family!");
//                    }
//
//                    if (number3 == 2) {
//                        System.out.print("\t-Enter family number(ID): ");
//                        int familyId = SCANNER.nextInt();
//                        System.out.print("\t-Enter all information about child. If you want to adopt boy press 1 or girl pres 2: ");
//                        int chosenNumber = SCANNER.nextInt();
//
//                        assert familyController != null;
//                        Family familyById = familyController.getFamilyById(familyId);
//
//                        if (chosenNumber == 1) {
//                            familyController.adoptChild(familyById, createMan());
//                        } else if (chosenNumber == 2) {
//                            familyController.adoptChild(familyById, createWoman());
//                        } else {
//                            System.out.println("Enter correct number!");
//                        }
//                        System.out.println("Congratulation, you adopted child to your family!");
//                    }
//
//                    break;
//
//                case "9":
//                    System.out.println("-9. Remove all children over age: ");
//                    System.out.print("\t-Enter your age: ");
//                    int age = SCANNER.nextInt();
//                    try {
//                        assert familyController != null;
//                        familyController.deleteAllChildrenOlderThen(age);
//                    } catch (Error e) {
//                        e.printStackTrace(); }
//            }
//
//        } while (!command.equalsIgnoreCase("exit"));
//


        Console console = new Console();
        console.consoleRun();

//

    }

}
