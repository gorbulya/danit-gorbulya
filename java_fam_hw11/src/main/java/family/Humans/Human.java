package family.Humans;

import family.Enum.DayOfWeek;
import family.Pets.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

public class Human {

    static {
        System.out.printf("Загружается новый класс : %s \n" , Human.class.getSimpleName());
    }

    {
        System.out.printf("Cоздается новый объект : %s \n" , Human.class.getSimpleName());
    }

    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void simpleDateFormat(String s) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;
        try {
            date = (Date) simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        this.birthDate = date.getTime();
    }

    public void setBirthDate(String string) {
        simpleDateFormat(string);
    }

    public String getBirthDate() {
        Date date = new Date(birthDate);
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
        return dt.format(date);
    }

    public int getYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(this.birthDate));
        LocalDate birthDate = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthDate, now);

        return (int) period.getYears();

    }

    public String describeAge() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(this.birthDate));
        LocalDate birthDate = LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
        LocalDate now = LocalDate.now();
        Period period = Period.between(birthDate, now);
        return getName() + " " + getSurname() + ", тебе " +  period.getYears() + " года, " + period.getMonths() + " месяца, " + period.getDays() + " дней.";

    }



    public void setIq(int iq) {
        this.iq = iq;
    }

    public int getIq() {
        return iq;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }


    public Human() {
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, String birthDate) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat(birthDate);
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat(birthDate);
        this.iq = iq;
    }


    public Human(String name, String surname, String birthDate, int iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        simpleDateFormat(birthDate);
        this.iq = iq;
        this.schedule = schedule;
    }

    public void greetPet(Pet pet) {
        System.out.printf("Привет, %s!%n", pet.getNickname());
    }

    public void describePet(Pet pet) {
        System.out.printf("У меня есть %s, ему %d лет, он %s%n", pet.getSpecies(), pet.getAge(), (pet.getTrickLevel() <= 50 ? "нехитрый" : "хитрый"));
    }

    public boolean feedPet(boolean time, Pet pet){
        int rand = (int)(Math.random()*100);
        if (time||(pet.getTrickLevel()>rand)) {
            System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname() );
            return true;
        } else {
            System.out.println("Думаю, Джек не голоден.");
            return false;
        }
    }

    public void finalize() {
        System.out.println(name + " " + surname + ". This human is removed.");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }


    @Override
    public String toString() {
        return "Human{name=" + name +
                ", surname=" + surname +
                ", birthDate=" + getBirthDate() +
                ", iq=" + iq +
                ", schedule=" + schedule + "}";
    }

}
