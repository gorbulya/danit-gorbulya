package family.Humans;

import family.Enum.DayOfWeek;
import family.Pets.Pet;

import java.util.Map;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname) {
        super(name, surname);
    }

    public Woman(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, birthDate, iq, schedule);
    }

    public void makeup() {
        System.out.println("Make up");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("HI, " + pet.getNickname());
    }
}
