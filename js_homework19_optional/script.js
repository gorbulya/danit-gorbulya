//Задание
//Реализовать функцию, которая позволит оценить, успеет ли команда разработчиков сдать проект до наступления дедлайна. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
//Технические требования:
//
//Функция на вход принимает три параметра:
//
//массив из чисел, который представляет скорость работы разных членов команды. Количество элементов в массиве означает количество человек в команде. Каждый элемент означает сколько стори поинтов (условная оценка сложности выполнения задачи) может выполнить данный разработчик за 1 день.
//массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить). Количество элементов в массиве означает количество задач в беклоге. Каждое число в массиве означает количество стори поинтов, необходимых для выполнения данной задачи.
//дата дедлайна (объект типа Date).
//
//
//После выполнения, функция должна посчитать, успеет ли команда разработчиков выполнить все задачи из беклога до наступления дедлайна (работа ведется начиная с сегодняшнего дня). Если да, вывести на экран сообщение: Все задачи будут успешно выполнены за ? дней до наступления дедлайна!. Подставить нужное число дней в текст. Если нет, вывести сообщение Команде разработчиков придется потратить дополнительно ? часов после дедлайна, чтобы выполнить все задачи в беклоге
//
// Работа идет по 8 часов в день по будним дням

// функция нумерация воскресения числом 7
function numberDay(number) {
    if (number === 0)
    {
        return 7;
    }
    return number;
}

//функция вычисления количества рабочих дней в заданом промежутке
function getWorkDay(startDay,finishDay){

let daysLag = Math.ceil(Math.abs(finishDay.getTime() - startDay.getTime()) / (1000 * 3600 * 24))+1; //всего дней между датами
let numberFirstDay = numberDay(startDay.getDay()); //номер первого дня
let numberLastDay = numberDay(finishDay.getDay());  // номер последнего дня
let countDayFirstWeek = 8-numberFirstDay; //общее количество дней первой недели
let countDayLastWeek = numberLastDay; // общее количество дней последней недели

//количество рабочих дней первйо недели
let workDayFirstWeek;
if ( numberFirstDay === 0 || numberFirstDay === 6){
    workDayFirstWeek = 0;
} else {
    workDayFirstWeek = 6-numberFirstDay;
}
//количество рабочих дней последней недели
let workDayLastWeek;

if ( numberLastDay === 0 || numberLastDay === 6){
    workDayLastWeek = 5;
} else {
    workDayLastWeek = numberLastDay;
}
// количество рабочих дней полных недель
let countFullWeekDay = (daysLag - countDayFirstWeek - countDayLastWeek)/7*5;
//  return количество всего рабочих дней

return  (countFullWeekDay + workDayFirstWeek + workDayLastWeek);

}

//функция вычисления количества сторипоинтов всех девелоперов в день и вычисления суммы сторипоинтов всех задач
function summStorypoint(develop){
    let res = 0;
    for (let i=0; i<develop.length; i++)
    {
        res += develop[i];
    }

    return res;
}


//начальные данные
let developStoryPoints = [10]//[7,8,8,3,4,5];
let backLog = [23]//[14,8,3,12,18,20,1,15];
let finDay = new Date('2020-04-30');
let startDay = new Date('2020-04-26');




function successCount(dev,log,startDay,finDay) {

    let allWorkDay =  getWorkDay(startDay,finDay);
    let devStoryPoint = summStorypoint(dev);
    let backLog = summStorypoint(log);

    if ((backLog/devStoryPoint) <= allWorkDay) {
        return "Все задачи будут успешно выполнены за " + (allWorkDay - Math.ceil(backLog/devStoryPoint) ) + " рабочих дней до наступления дедлайна!"
    } else if ((backLog/devStoryPoint) > allWorkDay ){
        return "Команде разработчиков придется потратить дополнительно "  + Math.ceil((backLog - allWorkDay*devStoryPoint)/devStoryPoint*8)  +  " часов после дедлайна, чтобы выполнить все задачи в беклоге";
    }
}

console.log(summStorypoint(developStoryPoints));
console.log(summStorypoint(backLog));
console.log(finDay);
console.log(startDay);
console.log(getWorkDay(startDay,finDay));
console.log(successCount(developStoryPoints,backLog,startDay,finDay));