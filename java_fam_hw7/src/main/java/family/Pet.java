package family;

import java.util.Arrays;
import java.util.Objects;
import java.util.Set;

abstract class Pet {

    static {
        System.out.printf("Загружается новый класс : %s \n" , Pet.class.getSimpleName());
    }

    {
        System.out.printf("Cоздается новый объект : %s \n" , Pet.class.getSimpleName());
    }

    protected Species species = Species.UNKNOWN;
    protected String nickname;
    protected int age;
    protected int trickLevel;
    protected Set<String> habits;

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }


    public Pet() {
    }

    public Pet(String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    void eat(){
        System.out.println("Я кушаю");
    }

    abstract void respond();

    void foul(){
        System.out.println("Нужно хорошо замести следы...");
    }

    public void finalize() {
        System.out.println(species + " " + nickname + ". This pet is removed.");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname);
    }


    @Override
    public String toString() {
        return species + "{CanFly="+ species.getCanFly() + ", NumberOfLegs=" + species.getNumberOfLegs() + ", HasFur=" + species.getHasFur() + "}" + "{nickname='" + nickname + "', age=" + age + ", trickLevel=" +
                trickLevel + ", habits=" +  habits + "}";
    }



}
