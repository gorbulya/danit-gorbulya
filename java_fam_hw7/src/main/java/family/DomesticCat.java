package family;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {

    {
        super.setSpecies(Species.CAT);
    }

    public DomesticCat() {
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Mrr mrr mrr");
    }

    @Override
    public void foul() {
        System.out.println("maaaauuu!");
    }
}
