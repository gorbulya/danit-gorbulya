package family;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class main {

    public static void main(String[] args) {

        Family family = new Family(new Human("Artem", "Ivanov", 35 ), new Human("Galina", "Ivanova", 36, 81));

//       for (int i = 0; i < 50000; i++) {
//           Human human1 = new Human("Artem", "Ivanov", 35 );
//       }


        Human child = new Human("Maxim", "Ivanov", 16);
        Human child2 = new Human();
        Human child3 = new Human("Petya", "Ivanov", 5, 30);

        Map<DayOfWeek, String> shedule = new HashMap<>();
        shedule.put(DayOfWeek.MONDAY,"пора в школу");
        shedule.put(DayOfWeek.THURSDAY,"пора на кружок");


        Human child4 = new Human("MAria", "Ivanova", 5, 80, shedule);


        Set<String> habits = new HashSet<>();
        habits.add("есть");
        habits.add("спать");
        habits.add("играть");

        Pet dog = new Dog( "Rex", 9, 30, habits);

        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        family.setPet(pets);

        System.out.println(family);
        System.out.print("Количество человек в семье: ");
        System.out.println(family.countFamily());

        family.deleteChild(child2);

        System.out.println(family);
        System.out.print("Количество человек в семье: ");
        System.out.println(family.countFamily());

        child2.feedPet(true, dog);
        child2.greetPet(dog);
        child2.describePet(dog);

        dog.eat();
        dog.respond();
        dog.foul();

        family.getMother().feedPet(true, dog);
        family.getMother().feedPet(false, dog);
        family.getMother().feedPet(false, dog);
        family.getMother().feedPet(false, dog);


        Man man = new Man("Stas", "Danilov", 16);
        Woman woman = new Woman("Nata", "Danilova", 18);
        man.repairCar();
        man.greetPet(dog);
        woman.makeup();
        woman.greetPet(dog);
    }

}
