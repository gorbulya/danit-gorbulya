package family;

import java.util.Set;

public class Dog extends Pet implements Foul{

    {
        super.setSpecies(Species.DOG);
    }

    public Dog() {
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Gav-gav");
    }

    @Override
    public void foul() {
        System.out.println("Play with me owner");
    }
}

