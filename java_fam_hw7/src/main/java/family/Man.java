package family;

import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    void repairCar() {
        System.out.println("Repair CAr");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("Hello, " + pet.getSpecies());
    }


}
