package family;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class HumanTest {

    Map<DayOfWeek, String> shedule = new HashMap<>();

    Human human = new Human("MAria", "Ivanova", 5, 80, shedule);

    @Before
    public void setAll(){
        shedule.put(DayOfWeek.MONDAY,"пора в школу");
        shedule.put(DayOfWeek.THURSDAY,"пора на кружок");
    }

    public String getExpectedToString(Human human) {
        return "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", year=" + human.getYear() +
                ", iq=" + human.getIq() +
                ", schedule=" + human.getSchedule() + "}";
    }

    @Test
    public void testToString(){
        assertEquals(human.toString(), getExpectedToString(human));
    }

}