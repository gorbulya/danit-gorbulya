package family;


import family.Pets.Dog;
import family.Pets.Pet;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;


public class PetTest {

    private Set<String> habits = new HashSet<>();

    private Dog pet = new Dog("Tuzyk", 5, 50, habits);

    public String getExpectedToString(Pet pet) {
        return pet.getSpecies() + "{CanFly="+ pet.getSpecies().getCanFly() + ", NumberOfLegs=" + pet.getSpecies().getNumberOfLegs() + ", HasFur=" + pet.getSpecies().getHasFur() + "}" + "{nickname='" + pet.getNickname() + "', age=" + pet.getAge() + ", trickLevel=" +
                pet.getTrickLevel() + ", habits=" + pet.getHabits() + "}";
    }

    @Before
    public void setAll(){
        habits.add("есть");
        habits.add("спать");
        habits.add("играть");
    }

    @Test
    public void testToString(){
        assertEquals(pet.toString(), getExpectedToString(pet));
    }


}
