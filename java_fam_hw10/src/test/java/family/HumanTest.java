package family;

import family.Enum.DayOfWeek;
import family.Humans.Human;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;


public class HumanTest {

    Map<DayOfWeek, String> shedule = new HashMap<>();

    Human human = new Human("MAria", "Ivanova", "01/03/2016", 80, shedule);

    @Before
    public void setAll(){
        shedule.put(DayOfWeek.MONDAY,"пора в школу");
        shedule.put(DayOfWeek.THURSDAY,"пора на кружок");
    }

    public String getExpectedToString(Human human) {
        return "Human{name=" + human.getName() +
                ", surname=" + human.getSurname() +
                ", birthDate=" + human.getBirthDate() +
                ", iq=" + human.getIq() +
                ", schedule=" + human.getSchedule() + "}";
    }

    @Test
    public void testToString(){
        assertEquals(human.toString(), getExpectedToString(human));
    }

}