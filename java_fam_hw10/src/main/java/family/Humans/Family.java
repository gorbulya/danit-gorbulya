package family.Humans;

import family.Pets.Pet;

import java.util.*;

public class Family {

    static {
        System.out.printf("Загружается новый класс : %s \n" , Family.class.getSimpleName());
    }

    {
        System.out.printf("Cоздается новый объект : %s \n" , Family.class.getSimpleName());
    }

    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pet;

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = new ArrayList<>();
        this.pet = new HashSet<>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public void addChild(Human human){
        children.add(human);
        human.setFamily(this);

    }

    public int countFamily(){
        int count = 2 + children.size();
        return count;
    }

    public void deleteChild(Human child) {
        if (children.size() > 0 && children.indexOf(child) != -1) {
            children.remove(child);
            child.setFamily(null);
        }

    }

    public boolean deleteChild(int index) {
        if(index > children.size()-1 || index < 0) return false;
        children.get(index).setFamily(null);
        children.remove(index);
        return true;
    }


    public void finalize() {
        System.out.println("Family removed!");
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }



    @Override
    public String toString() {
        return "Family{" + "\n" +
                "mother=" + mother + "\n" +
                ", father=" + father + "\n" +
                ", children=" + children + "\n" +
                ", pet=" + pet + "\n" + "}";
    }


}
