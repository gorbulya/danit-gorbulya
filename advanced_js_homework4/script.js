// Написать программу "Я тебя по айпи вычислю"

// Технические требования:

// Создать простую HTML страницу с кнопкой Вычислить по IP.
// По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
// Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
// Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.

const _URL_IP = 'https://api.ipify.org/?format=json';
const _URL_INFO = 'http://ip-api.com/json/';

let button = document.getElementById('container__button');
let textBox = document.getElementsByClassName('container_text')[0];

button.addEventListener("click", getDataByIp);



function getDataByIp() {

    fetch(`${_URL_IP}`)
        .then(r => {
            if (r.status === 200) {
                return r.json();
            } else {
                throw new Error(r.status);
            }
        })
        .then(r => {

            let ip = r.ip;
            console.log(ip);

            fetch(`${_URL_INFO}${ip}?fields=continent,country,regionName,city,district`)
                .then(r => {
                    if (r.status === 200) {
                        return r.json();
                    } else {
                        throw new Error(r.status);
                    }
                })
                .then(r => {
                    let adress = `Континент: ${r.continent} / Страна: ${r.country} / Регион: ${r.regionName} / Город: ${r.city} / Район: ${r.district}`;
                    textBox.innerHTML = adress;
                });
        })
        .catch(e => console.error(e));

}

