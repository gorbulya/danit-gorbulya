package family.Humans;

import family.Enum.DayOfWeek;
import family.Pets.Pet;

import java.util.Map;

public final class Woman extends Human {
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public void makeup() {
        System.out.println("Make up");
    }

    @Override
    public void greetPet(Pet pet) {
        System.out.println("HI, " + pet.getNickname());
    }
}
