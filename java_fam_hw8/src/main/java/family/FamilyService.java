package family;

import family.DAO.FamilyDao;
import family.Humans.Family;
import family.Humans.Human;
import family.Humans.Man;
import family.Humans.Woman;
import family.Pets.Pet;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        String result = "";
        System.out.println("List of all families: ");
        for (Family family : familyDao.getAllFamilies()) {
            result = (familyDao.getAllFamilies().indexOf(family) + 1) + ". " + family;
            System.out.println(result);
        }
    }

    public void getFamiliesBiggerThan(int number) {
        String result = "";
        int count = 1;
        System.out.println("Family bigger than " + number + " people:");
        for (Family family : familyDao.getAllFamilies()) {
            if (number < family.countFamily()) {
                result = (count++ + ". " + family);
                System.out.println(result);
            }
        }
    }

    public void getFamiliesLessThan(int number) {
        String result = "";
        int count = 1;
        System.out.println("Family less than " + number + " people:");
        for (Family family : familyDao.getAllFamilies()) {
            if (number > family.countFamily()) {
                result = (count++ + ". " + family);
                System.out.println(result);
            }
        }
    }


    public void countFamiliesWithMemberNumber(int number) {
        String result = "";
        int count = 1;
        System.out.println("Family number of people equal " + number + ":");
        for (Family family : familyDao.getAllFamilies()) {
            if (number == family.countFamily()) {
                result = (count++ + ". " + family);
                System.out.println(result);
            }
        }
    }


    public void createNewFamily(Human human1, Human human2) {
        familyDao.saveFamily(new Family(human1, human2));
    }

    public void deleteFamilyByIndex(int index) {
        if (index - 1 < 0 || index - 1 > familyDao.getAllFamilies().size()-1) {
        } else familyDao.deleteFamily(index-1);
    }

    public Family bornChild(Family family, String nameMan, String nameWoman) {
        int random = (int) (Math.random() * 2);
        Human man = new Man(nameMan, family.getFather().getSurname(), 0);
        Human woman = new Woman(nameWoman, family.getFather().getSurname(), 0);
        if (random == 0) {
            family.addChild(man);
            familyDao.saveFamily(family);
        } else {
            family.addChild(woman);
            familyDao.saveFamily(family);
        }
        return family;
    }

    public Family adoptChild(Family family, Human human) {
        human.setFamily(family);
        human.setSurname(family.getFather().getSurname());
        family.addChild(human);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        int resultAge;

        for (Family family : familyDao.getAllFamilies()) {
            Iterator iterator = family.getChildren().iterator();
            while (iterator.hasNext()) {
                Human child = (Human) iterator.next();


                    if (child.getYear() > age) {
                        iterator.remove();
                        familyDao.saveFamily(family);
                    }

            }
        }
    }

    public int count() {
        int result = 0;
        for (Family aFamily : familyDao.getAllFamilies()) {
            result++;
        }
        return result;
    }

    public Family getFamilyById(int indexFamily) {
        System.out.println("Family by id " + indexFamily + ":");
        return familyDao.getAllFamilies().get(indexFamily - 1);
    }

    public Set<Pet> getPets(int indexFamily) {
        System.out.println("Pets family by index " + indexFamily + ":");
        return familyDao.getFamilyByIndex(indexFamily - 1).getPet();
    }

    public void addPet(int indexFamily, Pet pet) {
        Family familyByIndex = familyDao.getFamilyByIndex(indexFamily - 1);
        familyByIndex.getPet().add(pet);
        familyDao.saveFamily(familyByIndex);
    }


}
