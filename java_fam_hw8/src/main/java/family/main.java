package family;

import family.DAO.CollectionFamilyDao;
import family.DAO.FamilyDao;
import family.Enum.DayOfWeek;
import family.Humans.Family;
import family.Humans.Human;
import family.Humans.Man;
import family.Humans.Woman;
import family.Pets.Dog;
import family.Pets.DomesticCat;
import family.Pets.Pet;
import family.Pets.RoboCat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class main {


    public static void main(String[] args) {

        Human child1 = new Man("Vlad", "Petrov", 5);
        Human child2 = new Man("Nick", "Ivanov", 10);
        Human child3 = new Man("Denis", "Sidorov", 15);
        Human child33 = new Man("Roma", "Konev", 20);
        Human woman = new Woman("Artem", "Horbulia", 45);
        Human man = new Man("Ksenia", "Horbulia" , 55);
        Family family = new Family(woman, man);
        family.addChild(child1);

        Human woman1 = new Woman("Kate", "Drushko", 41);
        Human man1 = new Man("Bob", "Drushko", 42);
        Family family1 = new Family(woman1, man1);
        family1.addChild(child2);

        Human woman2 = new Woman("Anne", "Poroshko", 66);
        Human man2 = new Man("Oleg", "Poroshko", 67);
        Family family2 = new Family(woman2, man2);
        family2.addChild(child3);
        family2.addChild(child33);

        Human woman3 = new Woman("Olga", "Romanova", 34);
        Human man3 = new Man("Danil", "Romanov", 35);
        Family family3 = new Family(woman3, man3);

        System.out.println(family.countFamily());
        System.out.println(family1.countFamily());
        System.out.println(family2.countFamily());
        System.out.println(family3.countFamily());
        System.out.println();

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);
        familyDao.saveFamily(family2);
        familyDao.saveFamily(family3);

        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.displayAllFamilies();
        System.out.println();

        familyController.getFamiliesBiggerThan(3);
        System.out.println();

        familyController.getFamiliesLessThan(3);
        System.out.println();

        familyController.countFamiliesWithMemberNumber(2);

        Human woman4 = new Woman("Taras", "Shevchenko", 55);
        Human man4 = new Man("Kristina", "Shevchenko", 55);

        familyController.createNewFamily(woman4, man4);
        System.out.println();

        familyController.displayAllFamilies();
        familyController.deleteFamilyByIndex(5);
        familyController.displayAllFamilies();
        System.out.println();

        familyController.bornChild(family, "Egor", "Maria");
        System.out.println(family);
        System.out.println();

        familyController.adoptChild(family, new Man("Diana", "ffff", 23));
        System.out.println(family.getChildren());
        System.out.println();

        System.out.println(familyController.getAllFamilies());
        familyController.deleteAllChildrenOlderThen(20);
        System.out.println(familyController.getAllFamilies());

        familyController.displayAllFamilies();
        System.out.println();
        System.out.println(familyController.count());

        familyController.displayAllFamilies();
        System.out.println(familyController.getFamilyById(3));

        Set<Pet> pets = new HashSet<>();

        Set<String> habits = new HashSet<>();
        habits.add("есть");
        habits.add("спать");
        habits.add("играть");

        Pet pet = new Dog( "Rex", 9, 30, habits);
        Pet pet1 = new DomesticCat("Izi", 5, 50, habits);
        Pet pet2 = new RoboCat("Robo", 1, 30, habits);
        pets.add(pet);
        pets.add(pet1);

        family.setPet(pets);
        System.out.println(familyController.getPets(1));
        System.out.println(familyController.getFamilyById(1));

        familyController.addPet(1, pet2);
        System.out.println(familyController.getFamilyById(1));

    }

}
