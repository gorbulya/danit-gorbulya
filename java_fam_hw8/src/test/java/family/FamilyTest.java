package family;

import family.Enum.DayOfWeek;
import family.Humans.Family;
import family.Humans.Human;
import family.Pets.Dog;
import family.Pets.Pet;
import org.junit.Before;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

import java.util.*;

public class FamilyTest {

    Family family = new Family(new Human("Artem", "Ivanov", 35 ), new Human("Galina", "Ivanova", 36, 81));

    Human child1 = new Human("Maxim", "Ivanov", 16);
    Human child2 = new Human("Petya", "Ivanov", 5, 30);

    private Map<DayOfWeek, String> shedule = new HashMap<>();

    Human child3 = new Human("MAria", "Ivanova", 5, 80, shedule);

    private Set<String> habits = new HashSet<>();
    Pet dog = new Dog( "Rex", 9, 30, habits);

    Set<Pet> pets = new HashSet<>();


    @Before
    public void setAll() {
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        habits.add("есть");
        habits.add("спать");
        habits.add("играть");
        shedule.put(DayOfWeek.MONDAY,"пора в школу");
        shedule.put(DayOfWeek.THURSDAY,"пора на кружок");
        pets.add(dog);
        family.setPet(pets);
    }

    public String getExpectedToString(Family family) {
        return "Family{" + "\n" +
                "mother=" + family.getMother() + "\n" +
                ", father=" + family.getFather() + "\n" +
                ", children=" + family.getChildren() + "\n" +
                ", pet=" + family.getPet() + "\n" + "}";
    }

    @Test
    public void testToString(){
        assertEquals(family.toString(), getExpectedToString(family));
    }

    @Test
    public void testCountFamily() {
        int resultCount = family.countFamily();
        family.addChild(new Human());
        assertEquals(resultCount+1, family.countFamily());
        family.addChild(new Human());
        assertEquals(++resultCount+1, family.countFamily());
    }

    @Test
    public void testAddChildPositive() {
        int resultBefore = family.getChildren().size();
        Human newChild = new Human("Nazar", "Ivanov", 34);
        family.addChild(newChild);

        assertEquals(family.getChildren().size(), resultBefore+1);
        assertEquals(family.getChildren().get(family.getChildren().size()-1), newChild);
    }

    @Test
    public void testDeleteChildPositive() {
        int resultBefore = family.getChildren().size();
        family.deleteChild(child2);
        assertEquals(family.getChildren().size(), resultBefore-1);

        for (Human child : family.getChildren()) {
            assertNotSame(child, child2);
        }
    }

    @Test
    public void testDeleteChildNegative() {
        List<Human> resultBefore = family.getChildren();
        family.deleteChild(new Human("Injdsjjsd", "asdasdlasd", 56));
        assertEquals(resultBefore, family.getChildren());
    }

    @Test
    public void testDeleteChildIndexPositive() {
        int resultBefore = family.getChildren().size();
        boolean b = family.deleteChild(1);
        assertEquals(family.getChildren().size(), resultBefore-1);
        assertEquals(b,true);

        for (Human child : family.getChildren()) {
            assertNotSame(child, child2);
        }
    }

    @Test
    public void testDeleteChildIndexNegative() {
        int resultBefore = family.getChildren().size();
        List<Human> resultBeforeStr = family.getChildren();
        boolean b = family.deleteChild(-1);
        assertEquals(family.getChildren().size(), resultBefore);
        assertEquals(b,false);
        assertEquals(resultBeforeStr, family.getChildren());
    }








}
