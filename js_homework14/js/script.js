$(document).ready(function () {


    $(document).ready(function(){
        $("#menu").on("click","a", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
                top = $(id).offset().top;
            $('body,html').animate({scrollTop: top}, 1500);
        });
    });


    $('#inTop').click(function (event) {
        event.preventDefault();
        $(document.documentElement).animate({
            scrollTop: 0
        }, 3000);
    });

    $(document).scroll(function (event) {

        const scrolling = window.scrollY;
        const windowHeight = window.innerHeight;

        if(scrolling > windowHeight){
            $('#inTop').fadeIn('300');
        } else {
            $('#inTop').fadeOut('300');
        }
    });

    $('#hide-button').click(function () {
        $('#hide-div').slideToggle(500);
    })



});